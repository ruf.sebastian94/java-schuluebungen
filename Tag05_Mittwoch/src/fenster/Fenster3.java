package fenster;

import java.awt.Color;

/*
	Wir erzeugen eine universelle Klasse MyFrame, 
	die immer wieder benutzbar ist.
	
	Sie enth�lt viele �berladungen f�r sehr einfache 
	und komplexe Fenster.
*/

public class Fenster3 {

	public Fenster3() {

		// MyFrame f1 = new MyFrame(); f1.setVisible(true);
		// MyFrame f2 = new MyFrame(200, 200); f2.setVisible(true);
		// MyFrame f3 = new MyFrame(500, 200, "Marzipanschwein"); f3.setVisible(true);
		
		MyFrame f4 = new MyFrame(500, 200, "Marzipanschwein", Color.PINK); 
		f4.setVisible(true);
	}

	public static void main(String[] args) {
		new Fenster3();

	}

}
