package fenster;

import javax.swing.JFrame;

/*
	Hier wird ein Fenster erzeugt, indem wir  
	von der Klasse JFrame erben. 

    Erinnern Sie sich:  public class Form2 : Form in C# ?

*/



public class Fenster2 extends JFrame {

	public Fenster2() {
				
		
		// Eigenschaften zuweisen:
		this.setBounds(500, 300, 335, 180); // x, y, breite, h�he
		this.setTitle("Mein Fenster Nr. 2");
		this.setLayout(null);               // Jetzt k�nnen Controls �ber x-y-Koordinaten platziert werden
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// sichtbar machen:
		this.setVisible(true);
		
	}

	public static void main(String[] args) {
		new Fenster2();
	}

}
