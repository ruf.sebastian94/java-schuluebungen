package fenster;

import javax.swing.JFrame;

/*
	Hier wird ein Fenster erzeugt, indem wir die Klasse 
	JFrame instanziieren. 
*/

public class Fenster1 {

	public Fenster1() {
		
		// Instanziierung:
		JFrame frm = new JFrame();
		
		// Eigenschaften zuweisen:
		frm.setBounds(500, 300, 335, 180); // x, y, breite, h�he
		frm.setTitle("Mein Fenster");
		frm.setLayout(null);               // Jetzt k�nnen Controls �ber x-y-Koordinaten platziert werden
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// sichtbar machen:
		frm.setVisible(true);
		
		// vertikales und horizontales Zentrieren des Fensters:
		frm.setLocationRelativeTo(null);
	}

	public static void main(String[] args) {
		new Fenster1();
	}

}
