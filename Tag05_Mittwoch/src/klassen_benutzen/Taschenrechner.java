package klassen_benutzen;

import java.util.Scanner;

public class Taschenrechner {

	// Instanziierung der Klasse Rechnen:
	Rechnen r = new Rechnen();
	
	
	public Taschenrechner() {
		
		Scanner s = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		int a = s.nextInt();
		
		System.out.print("Bitte geben Sie eine noch eine ganze Zahl ein: ");
		int b = s.nextInt();
		
		System.out.println(a + " + " + b + " = " + r.addieren(a, b));
		System.out.println(a + " - " + b + " = " + r.subtrahieren(a, b));
		System.out.println(a + " * " + b + " = " + r.multiplizieren(a, b));
		System.out.println(a + " : " + b + " = " + r.dividieren(a, b) );
		
		s.close();
		
	}

	public static void main(String[] args) {
		new Taschenrechner();
	}

}
