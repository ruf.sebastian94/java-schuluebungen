package klassen_benutzen;

public class Rechnen {

	public int addieren(int a,  int b) {
		return a + b;
	}
	
	public int subtrahieren(int a,  int b) {
		return a - b;
	}
	
	public int multiplizieren(int a,  int b) {
		return a * b;
	}
	
	public double dividieren(int a,  int b) {
		return (double)a / (double)b;
	}

}
