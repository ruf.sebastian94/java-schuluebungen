package testpackage;

import fenster.MyFrame; 


/* 
  In dieser Klasse wird gezeigt, wie man eine Java-Klasse
  aus einem anderen Package einbindet. 
*/

public class Testklasse {

	public Testklasse() {
		MyFrame f = new MyFrame(900, 100);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Testklasse();
	}

}
