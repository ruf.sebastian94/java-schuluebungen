package testpackage;

import java.awt.Color;
import java.util.Random;

import fenster.MyFrame; // Achtung: selbes Projekt, also: Package.Klasse

/* 
 
  Erstellen sie eine Klasse "Zufallsfenster" im Package testpackage
  nach Start der Klasse soll eine zuf�llige Zahl aus dem Bereich [5,30] 
  an Fenstern auf dem Desktop:
  an zuf�lligen Postitionen, 
  mit zuf�lligen Breiten (Minimum ist 50px) 
  und zuf�lligen H�hen (Minimum ist 50px)
  mit zuf�lliger Hintergrundfarbe entstehen (rot,gr�n,blau) 
                            Farb-Beispiel: Color hintergrundfarbe = new Color(255,0,0);
    
*/

public class Zufallsfenster {

	public Zufallsfenster() {

		Random r = new Random();
		
		for (int i = 0; i < r.nextInt(26) + 5; i++) {
		
			int x = r.nextInt(1400); // x-Position
			int y = r.nextInt(800);  // y-Position
			int breite = r.nextInt(500) + 50;
			int hoehe = r.nextInt(400) + 50;
			Color hintergrundfarbe = new Color(r.nextInt(256), r.nextInt(256), r.nextInt(256));
			
			MyFrame f = new MyFrame(x, y, breite, hoehe, hintergrundfarbe);
			f.setVisible(true);
		}
	}

	
	public static void main(String[] args) {
		new Zufallsfenster();
	}

}
