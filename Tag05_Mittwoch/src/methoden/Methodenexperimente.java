package methoden;

import java.util.Random;

public class Methodenexperimente {
	
	// Bereich f�r globale Variablen = Eigenschaften = Felder/Datenfelder = Klassenvariablen:
	int i = 15;
	

	// constructor
	public Methodenexperimente() {
	
		meineErsteMethode(); // Methodenaufruf der 1. �berladung
		meineErsteMethode(4.123); // Methodenaufruf der 2. �berladung
		meineErsteMethode(42); // Methodenaufruf der 3. �berladung
		
		
		// Berechnung des Volumens von 6 gleichartigen Aquarien:
		
		int anzahl = 6;
		double breite = 20.5; // cm
		double hoehe = 14.5;  // cm
		double tiefe = 14.5;  // cm
				
		double wassermenge = meineZweiteMethode(anzahl, breite, hoehe, tiefe);
		
		wassermenge = Math.round(100.0 * wassermenge) / 100.0;
		
		System.out.println("Wassermenge: " + wassermenge + " Liter");
		
		System.out.println("i: " + i); // 15, da zu diesem Zeitpunkt nur die globale Variable i existiert
		
		int i = 42; // lokale Variable i
		System.out.println("i: " + i); //42 denn lokale Variable haben die h�here Priorit�t
		
		
		// Ziel: eine Methode gibt MEHRERE Werte zur�ck.
		// Erschaffen Sie eine Methode, die mehrere ganze Zufallszahlen liefert
		// Parameter der Methode: untere Grenze, obere Grenze, Anzahl!
		int[] zz = zufallszahlen(5, 10, 20);
		
		// Geben Sie hier die Zufallszahlen aus:
		for (int foo : zz) System.out.print(foo + " ");
		
	}
	
	private int[] zufallszahlen(int untereGrenze, int obereGrenze, int anzahl) {
		
		int[] i = new int[anzahl];
		
		Random r = new Random(); 
		
		for (int j = 0; j < i.length; j++) {
			i[j] = r.nextInt(obereGrenze + 1 - untereGrenze) + untereGrenze;
		}
		
		return i; // i ist ein Array
	}
	
	private double meineZweiteMethode(int anzahl, double breite, double hoehe, double tiefe) {
		double gesamtvolumen = anzahl * (breite/10.0) * (hoehe/10.0) *(tiefe/10.0);  
		return gesamtvolumen ;
	}

	private void meineErsteMethode() {
		System.out.println("in der ersten Methode, 1. �berladung");
	}
	
	private void meineErsteMethode(double p) // das p nennt man PARAMETER
    {
		System.out.println("in der ersten Methode, 2. �berladung " + p);
	}
	
	private void meineErsteMethode(int p) {
		System.out.println("in der ersten Methode, 3. �berladung " + p);
	}

	public static void main(String[] args) {
		new Methodenexperimente();
	}

}
