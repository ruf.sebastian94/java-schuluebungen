package informationen;

import java.awt.Toolkit;

public class InformationenHolen {

	
	public static void main(String[] args) {
	
		
		System.out.println("Name des OS: " + System.getProperty("os.name"));
		System.out.println("Rechner-Architektur: " + System.getProperty("os.arch"));
		System.out.println("OS Versionsnummer: " + System.getProperty("os.version"));
		System.out.println("Nutzername: " + System.getProperty("user.name"));
		System.out.println("Nutzerverzeichnis: " + System.getProperty("user.home"));
		System.out.println("das aktuelle Arbeitsverzeichnis: " + System.getProperty("user.dir"));
		System.out.println("Anzahl der Prozessor-Kerne: " + Runtime.getRuntime().availableProcessors());
		
		// Bildschirm-Daten:
		System.out.println("horizontale Aufl�sung: " + Toolkit.getDefaultToolkit().getScreenSize().getWidth());
		System.out.println("vertikale Aufl�sung: " + Toolkit.getDefaultToolkit().getScreenSize().getHeight());
		System.out.println("Aufl�sung: " + Toolkit.getDefaultToolkit().getScreenResolution() + " dpi");
	
	}

}
