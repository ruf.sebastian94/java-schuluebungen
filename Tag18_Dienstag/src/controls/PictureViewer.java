package controls;

import java.awt.BorderLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import meinzeug.MyFrame;

public class PictureViewer {

	MyFrame f = new MyFrame(700, 1000, "My Picture Viewer");

	public PictureViewer() {

		f.setLayout(new BorderLayout());

		double sf = 1.1; // Skalierungsfaktor

		ImageIcon icon = new ImageIcon("streetphoto.jpg");

		double bildbreite = icon.getIconWidth();
		double bildhoehe = icon.getIconHeight();

		int framebreite = f.getHeight();
		int framehoehe = f.getWidth();
		
		// Das Originalbild ist 1000x1500px.
		while (framebreite < (bildbreite / sf) || framehoehe < (bildhoehe / sf)) {
			sf = sf + 0.01;	
		}
		
		JOptionPane.showMessageDialog(null,"verbliebender Skalierungsfaktor: " + sf);
		
		bildbreite = bildbreite / sf;
		bildhoehe = bildhoehe/ sf;
		
		// Das Frame auf die Gr��e des skalierten Bildes schrumpfen lassen:
		f.setSize((int)bildbreite, (int)bildhoehe);
		
		JOptionPane.showMessageDialog(null,"Bildbreite: " + bildbreite);
		
		Image image = icon.getImage();
		Image newimg = image.getScaledInstance((int)bildbreite , (int)bildhoehe, java.awt.Image.SCALE_SMOOTH); // scale
																													// way
		icon = new ImageIcon(newimg);

		JLabel lbl = new JLabel(icon);
		f.add(lbl);

		f.setVisible(true);
	}

	public static void main(String[] args) {
		new PictureViewer();
	}

}
