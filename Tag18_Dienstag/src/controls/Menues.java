package controls;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import meinzeug.MyFrame;

public class Menues implements ActionListener {

	MyFrame f;
	JMenuItem itemNeu, itemOeffnen, itemSpeichern, itemBeenden, itemHandbuch;
	JCheckBox itemLabels, itemButtons, itemTextFields;
	JLabel lbl;
	JButton btn;
	JTextField txt;

	public Menues() {

		f = new MyFrame(600, 500, "Men�s");

		createMenus();
		createControls();

		f.setVisible(true);
	}

	private void createMenus() {

		// den Balken erschaffen:
		JMenuBar menubar = new JMenuBar();
		menubar.setBackground(new Color(192, 237, 204));

		// die Men�s "Datei", "Ansicht" und "Hilfe" erschaffen:
		JMenu menuDatei = new JMenu("Datei");
		JMenu menuAnsicht = new JMenu("Ansicht");
		JMenu menuHilfe = new JMenu("Hilfe");

		// Men�-Unterpunkte erschaffen und dem Men� "Datei" hinzuf�gen:
		itemNeu = new JMenuItem("Neu");
		itemOeffnen = new JMenuItem("�ffnen");
		itemSpeichern = new JMenuItem("Speichern");
		itemBeenden = new JMenuItem("Beenden");
		itemNeu.addActionListener(this);
		itemOeffnen.addActionListener(this);
		itemSpeichern.addActionListener(this);
		itemBeenden.addActionListener(this);

		// bei Men� Datei soll das D unterstrichen sein und 
		// das Men� soll sich �ffnen mit: ALT + D
		menuDatei.setMnemonic('D');
		
		menuDatei.add(itemNeu);
		menuDatei.add(itemOeffnen);
		menuDatei.add(itemSpeichern);
		menuDatei.add(new JSeparator()); // ein waagerechter Trennstrich
		menuDatei.add(itemBeenden);

		// Men�-Unterpunkte erschaffen und dem Men� "Ansicht" hinzuf�gen:
		itemLabels = new JCheckBox("Labels");
		itemButtons = new JCheckBox("Buttons");
		itemTextFields = new JCheckBox("Textfields");

		itemLabels.setSelected(true);
		itemButtons.setSelected(true);
		itemTextFields.setSelected(true);

		itemLabels.addActionListener(this);
		itemButtons.addActionListener(this);
		itemTextFields.addActionListener(this);

		menuAnsicht.add(itemLabels);
		menuAnsicht.add(itemButtons);
		menuAnsicht.add(itemTextFields);

		// Hilfe-Men�-Unterpunkte erschaffen und dem Men� "Hilfe" hinzuf�gen:
		itemHandbuch = new JMenuItem("Handbuch");
		itemHandbuch.addActionListener(this);

		// shortcut f�r "Handbuch" CTRL+H (bei Problemen: siehe n�chstes Kapitel)
		KeyStroke ctrlH = KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK);
		itemHandbuch.setAccelerator(ctrlH);

		menuHilfe.add(itemHandbuch);

		// Die Men�s "Datei", "Ansicht" und "Hilfe"
		// auf den Balken kleben:
		menubar.add(menuDatei);
		menubar.add(menuAnsicht);
		menubar.add(menuHilfe);

		f.setJMenuBar(menubar); // Den Balken auf das Frame setzen.
	}

	private void createControls() {

		txt = new JTextField("TextField");
		txt.setBounds(10, 200, 550, 30);

		// da Strg + H das Hilfe-Handbuch �ffnen soll,
		// m�ssen wir das Strg + H aus dem TextField "txt" entfernen:
		txt.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.CTRL_MASK), 
				                                     "none");

		f.add(txt);

		btn = new JButton("Button");
		btn.setBounds(10, 250, 550, 30);
		f.add(btn);

		lbl = new JLabel("Label");
		lbl.setBounds(10, 300, 550, 30);
		f.add(lbl);

	}

	public static void main(String[] args) {
		new Menues();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == itemBeenden) {
			System.exit(0);
		}

		if (e.getSource() == itemOeffnen) {

			JFileChooser fc = new JFileChooser();

			fc.showOpenDialog(null);
			String pfadUndDateiName = fc.getSelectedFile().getAbsoluteFile().toString();
			lbl.setText(pfadUndDateiName);
		}

		if (e.getSource() == itemLabels) {
			if (itemLabels.isSelected()) {
				lbl.setVisible(true);
			} else {
				lbl.setVisible(false);
			}
		}

		if (e.getSource() == itemButtons) {
			if (itemButtons.isSelected()) {
				btn.setVisible(true);
			} else {
				btn.setVisible(false);
			}
		}

		if (e.getSource() == itemTextFields) {
			if (itemTextFields.isSelected()) {
				txt.setVisible(true);
			} else {
				txt.setVisible(false);
			}
		}

		if (e.getSource() == itemHandbuch) {

			try {

				// Datei liegt im Projektordner und nicht auf dem Desktop!
				Desktop.getDesktop().open(new File("Handbuch.pdf"));

			} catch (Exception e2) {
				e2.printStackTrace();
				JOptionPane.showMessageDialog(null, "Problem");
			}
		}
	}

}
