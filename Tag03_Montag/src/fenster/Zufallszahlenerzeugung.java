package fenster;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Zufallszahlenerzeugung implements ActionListener {

	// Bereich f�r globale Variablen:
	JFrame frm;
	JButton btnVerarbeitung;
	JLabel lblAusgabe;
	
	
	// Konstruktor:
	public Zufallszahlenerzeugung() {

		createFrame();
		createControls();
		
		frm.setVisible(true);		
	}

	
	private void createFrame() {
		frm = new JFrame();
		frm.setBounds(500, 300, 310, 100); 	
		frm.setLayout(null);				
		frm.setTitle("Zufallszahlenerzeugung");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void createControls() {

		btnVerarbeitung = new JButton();
		btnVerarbeitung.setBounds(40, 10, 100, 30);
		btnVerarbeitung.setText("Erzeugen");
		btnVerarbeitung.addActionListener(this);
		frm.add(btnVerarbeitung);
				
		lblAusgabe = new JLabel();
		lblAusgabe.setBounds(170, 10, 300, 30);
		lblAusgabe.setFont(new Font("Arial", Font.PLAIN , 20));
		lblAusgabe.setText(". . .");
		frm.add(lblAusgabe);
	}


	// das ist der event handler des ActionListeners:
	@Override
	public void actionPerformed(ActionEvent e) {
		
		Random r = new Random();
		String strAusgabe = "";
		
		for (int i = 0; i < 3; i++) {
			strAusgabe += (10 + r.nextInt(11)) + "   ";
		}
		
		lblAusgabe.setText(strAusgabe );
	   
	}

	public static void main(String[] args) {
		new Zufallszahlenerzeugung(); // eigener Konstruktor
	}
}
