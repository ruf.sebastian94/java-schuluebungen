package arraylists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArrayListExperimente {
	/*
	 * Vorteil der ArrayList gegen�ber Array: kann beliebig wachsen Nachteil der
	 * ArrayList gegen�ber Array: nur eindimensional siehe Skript. S 56
	 */

	// Constructor:
	public ArrayListExperimente() {

		// Instanziierung einer ArrayList f�r Stringwerte
		ArrayList<String> lst = new ArrayList<String>();

		// einzelne Elemente zur Liste hinzuf�gen
		lst.add("Erdbeeren");
		lst.add("Birnen");
		lst.add("Aprikosen");
		lst.add("Pflaumen");

		// einen Sammelbeh�lter (Array) zur Liste hinzuf�gen:
		String[] beatles = { "John", "Paul", "George", "Ringo" };

		lst.addAll(Arrays.asList(beatles));

		// Anzahl aller Elemente ermitteln und ausgeben:
		System.out.println("Anzahl aller Elemente: " + lst.size());

		// auf ein einzelnes Element zugreifen:
		System.out.println("Element mit dem Index 3: " + lst.get(3));// Pflaumen

		// Index des Elementes "Aprikosen" ermitteln:
		System.out.println("Index des Elementes \"Aprikosen\": " + lst.indexOf("Aprikosen"));

		// Die Liste sortieren:
		Collections.sort(lst);

		// Ausgabe aller Elemente
		for (String foo : lst) {
			System.out.println(foo);
		}

		// Vorhandensein eines Elementes pr�fen:
		if (lst.contains("Oma")) {
			System.out.println("Oma ist in der Liste.");
		} else {
			System.out.println("Oma ist nicht in der Liste.");
		}
		
		if (lst.contains("Paul")) {
			System.out.println("Paul ist in der Liste.");
		}

	}

	public static void main(String[] args) {

		// wir rufen den eigenen Constructor auf, damit wir die
		// "statische Zwangsjacke" verlassen k�nnen.
		new ArrayListExperimente();

	}

}
