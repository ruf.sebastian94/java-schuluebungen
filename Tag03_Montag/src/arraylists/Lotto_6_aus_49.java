package arraylists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;

/*
	Erzeugen Sie im Package �ArrayLists� eine Klasse 
	�Lotto_6_aus_49�, die folgendes leistet: Als Konsolen-
	programm oder als JFrame-Programm sollen nach dem 
	Start 6 zuf�llige Ganzzahlen erzeugt und 
	(irgendwie) sortiert ausgegeben werden.

	Beachten Sie, dass es 6 unterschiedliche Zahlen sein m�ssen!
	Tipp: benutzen Sie .contains 
*/

public class Lotto_6_aus_49 {

	public Lotto_6_aus_49() {
		
		Random r = new Random();
		
		ArrayList<Integer> lst = new ArrayList<Integer>();
		
		while(lst.size() <= 6) {
			int zufallszahl = r.nextInt(49) + 1;
			if(!lst.contains(zufallszahl)) lst.add(zufallszahl);
		}
				
		// sortieren:
		lst.sort(null);
				
		for (int foo : lst) {
			System.out.print(foo + "  ");
		}
		
		
	}

	public static void main(String[] args) {
		new Lotto_6_aus_49();
	}

}
