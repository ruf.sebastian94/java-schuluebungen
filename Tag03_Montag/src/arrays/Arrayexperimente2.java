package arrays;

import java.util.Arrays;

public class Arrayexperimente2 {

	public static void main(String[] args) {

		// *******************************************************
		// eindimensionales Array (Vektor, Liste, Aufz�hlung)
		// *******************************************************

		// Deklaration des Arrays f�r 3 ganze Zahlen:
		int[] a = new int[3]; // Indizes 0, 1 und 2 (keine 3!)

		// Wertezuweisungen:
		a[0] = 5;
		a[1] = 130;
		a[2] = 42;

		// Ausgabe mit For-Next-Schleife
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}

		System.out.println("\n-----------------------");

		// Ausgabe mit For-Each-Schleife
		for (int foo : a) {
			System.out.print(foo + " ");
		}

		// ***********************************************
		// Eigenschaften bei Arrays:
		System.out.println();
		System.out.println("L�nge des Arrays: " + a.length);

		Arrays.sort(a);

		System.out.print("\n-------\nsortiert: ");

		// Ausgabe des sortierten Arrays mit For-Each-Schleife
		for (int foo : a) {
			System.out.print(foo + " ");
		}
		
		System.out.println("\n------------\n Initialisierung: ");// nur ein Zeilenumbruch
		
		// Initialisierung eine eindimensionalen Arrays
		int[] c = {12, 30, 100, 0, -55};
		// Ausgabe:
		for (int foo : c) System.out.print(foo + " ");
		
		

		// *******************************************************
		// zwei- und mehrdimensionale Arrays
		// zwei: Tabelle oder Matrix
		// drei und mehr: Matrix
		// *******************************************************

		// Deklaration
		int[][] b = new int[2][2];

		// Wertezuweisungen:
		b[0][0] = 5;
		b[0][1] = 13;
		b[1][0] = 42;
		b[1][1] = -11;

		// Ausgabe des Wertes b11:
		System.out.println("\n\n b11: " + b[1][1]);

		// �ndern eines Wertes: 5 --> 7 (b00)
		b[0][0] = 7;

		// Ausgabe des Wertes b00:
		System.out.println(" b00: " + b[0][0]);

		System.out.println("------------");// nur ein Zeilenumbruch
		
		// Ausgabe des gesamten Arrays:
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.print(b[i][j] + "  ");
			}
			System.out.println();// nur ein Zeilenumbruch
		}
		
		
		// Initialisierung eines zweidimensionalen Arrays
		String[][] vorname = {
				              {"John", "Paul"},
				              {"George", "Ringo"}
							 };
		
		// Ausgabe von vorname11:
		System.out.println(vorname[1][1]); // Ringo

	}

}
