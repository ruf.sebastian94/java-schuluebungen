package zufallszahlen;

public class Variante2 {

	// Variante2: �hnlich wie in C: Zufallzahlen im Bereich [0.0, 1.0[
	// Wir benutzen die Math-Klasse.

	public static void main(String[] args) {

		// eine Zufallszahl aus dem Bereich [0.0, 1.0[ ausgeben:
		System.out.println(Math.random());

		System.out.println("---------------------");

		// Lassen Sie untereinander 10 Zufallszahlen
		// aus dem Bereich [0.0, 1.0[ ausgeben:
		for (int i = 0; i < 10; i++) System.out.println(Math.random());

		System.out.println("---------------------");
		
		// Lassen Sie untereinander 10 Zufallszahlen, 
		// die ganzzahlig sind und aus dem Bereich [0, 10[
		for (int i = 0; i < 10; i++) {
			int zahl = (int) (Math.random() * 10);
			System.out.println(zahl);
		}
		
		
		
		
	}

}
