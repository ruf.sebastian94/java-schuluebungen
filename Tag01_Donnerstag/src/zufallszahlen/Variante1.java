package zufallszahlen;

import java.util.Random;

public class Variante1 {
	
	// Variante1: ähnlich wie in C# mit Instanz der Klasse Random

	public static void main(String[] args) {
		
		Random r = new Random();
		
		System.out.println(r.nextInt());     // Bereich [-MaxInt, +Maxint]
		System.out.println(r.nextInt(10));   // Bereich [0, 10[
		
		//System.out.println(Integer.MAX_VALUE);// <-- Die größtmögliche Integer-Zahl
	}

}
