package zufallszahlen;

import java.util.Random;

public class Statistik {

	public static void main(String[] args) {

		// Erzeugen Sie 10 ganzzahlige Zufallszahlen
		// aus dem Bereich [0, 20] und geben Sie diese
		// nebeneinander aus.
		// Geben Sie (unter einem Trennstrich)
		// die gr��te Zahl (=Maximum) 
		// sowie die kleinste Zahl (Minimum) 
		// sowie die Summe aller 10 Zufallszahlen
		// sowie den Durchschnitt (arithmetischer Mittelwert)
		// aus!

		Random r = new Random();	// die Instanz der Klasse Random
		int z; 						// Beh�lter f�r die Zufallszahl
		
		int maximum = 0;			// Beh�lter f�r das Maximum
		int minimum = 20;			// Beh�lter f�r das Minimum
		int summe = 0;				// Beh�lter f�r die Summe
		double durchschnitt;    	// Beh�lter f�r den arithmetischen Mittelwert
		
		for (int i = 0; i < 10; i++) {
			
			z = r.nextInt(21);
			System.out.print(z + " ");
			
			if(maximum < z) {maximum = z;}
			if(minimum > z) {minimum = z;}
			summe += z;
		}
		
		durchschnitt = summe / 10.0;  // bitte nach Verlassen der for-Schleife!
		
		System.out.println("\n____________________________________________________ "); 
		
		System.out.println("Maximum: " + maximum);
		System.out.println("Minimum: " + minimum);
		System.out.println("Summe: " + summe);
		System.out.println("Durchschnitt: " + durchschnitt);
		

	}

}
