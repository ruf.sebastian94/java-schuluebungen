package schleifen;

import java.util.Scanner;

public class Schleifenexperimente {

	public static void main(String[] args) {

		int i = 1;

		while (i <= 20) {
			System.out.print(i + " ");
			i++;
		}

		// ******************************************************

		System.out.println();
		i = 1;

		while (i <= 1024) {
			System.out.print(i + " ");
			i *= 2; // Verdoppeln des Wertes in i
		}

		// ******************************************************
		// Quadratzahlen ausgeben:

		System.out.println(); // Zeilenumbruch

		for (i = 1; i <= 7; i++) {
			System.out.print((int) Math.pow(i, 2) + " ");
		}

		/*
		 * Fakult�t ausgeben: Berechnen Sie mit Hilfe einer Schleife Ihrer Wahl,
		 * wieviele M�glichkeiten des Einlaufs von n L�ufern es in einem 100m-Lauf gibt:
		 * n! �n Fakult�t� Lassen Sie die Zahl f�r n vom User eingeben!
		 */

		System.out.println(); // Zeilenumbruch

		System.out.print("Geben Sie die Anzahl der L�ufer ein: ");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		int nFakultaet = 1;

		for (i = n; i > 1; i--) {
			nFakultaet *= i; 
		}

		System.out.println(n + "! = " + nFakultaet);
	}

}
