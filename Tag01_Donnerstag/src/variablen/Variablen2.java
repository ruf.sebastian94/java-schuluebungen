package variablen;

import java.util.Scanner;

public class Variablen2 {
	
	// in der Klasse Variablen2 t�tigen wir Eingaben
	// mit Hilfe von Variablen

	public static void main(String[] args) {
		/*
		System.out.print("Gib bitte Deinen Vornamen ein: ");
		
		Scanner myScanner1 = new Scanner(System.in);
		
		String s = myScanner1.nextLine();
		
		myScanner1.close(); // nicht vergessen!
		
		System.out.println("Hallo " + s + "!");
		*/
		
		/* ***************************************** */
		// Zahleneingabe mit Scanner:
		/*
		System.out.print("Bitte geben Sie eine Flie�kommazahl ein: ");
		
		Scanner myScanner2 = new Scanner(System.in);
		
		double z = myScanner2.nextDouble();
		
		myScanner2.close(); // nicht vergessen!
		
		System.out.println("Ihre Zahl: " + z + " zum Quadrat ist: " + Math.pow(z, 2) );
		
		*/
		
		// Einen Zahlenwert aus einem String holen: 
		
		System.out.print("Bitte geben Sie eine Flie�kommazahl ein: ");
		
		Scanner myScanner3 = new Scanner(System.in);
		
		String strZahl = myScanner3.nextLine();
		
		double d = Double.valueOf(strZahl); // Variante 1
		double e = Double.parseDouble(strZahl); // Variante 2
		
		System.out.println(d + " " + e);
		
		
		
		System.out.println();
		
		
	}

}
