package variablen;

public class Variablen1 {
	
	// Deklaration, Wertezuweisung und 
	// Initialisierung von Variablen
	
	public static void main(String[] args) {

		// Berechnungen mit 3 Variablen:

		// Deklaration von Ganzzahl-Variablen:
		int a, b, c;

		// Wertezuweisungen:
		a = 2;
		b = 3;
		
		// Wertezuweisung als Berechnung:
		c = a * b;
		
		// Ausgabe:
		System.out.println("Ergebnis: " + c);
		
		
		/* ***************************************** */
		
		// Initialisierung:
		double x = 3.5;
		double y = 1.3;
		double z = x / y;
				
		// Ausgabe:
		System.out.println(z);
		
	}

}
