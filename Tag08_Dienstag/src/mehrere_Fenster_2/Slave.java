package mehrere_Fenster_2;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class Slave extends JFrame implements WindowListener {
	
	// Bereich f�r Globale Variablen
	Master m;
	
	// Konstruktor:
	public Slave(Master m) {
		this.setBounds(1200, 300, 300, 300); // x, y, breite, h�he 
		this.setTitle("Slave");
		this.m = m;
		this.addWindowListener(this);
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		m.btnOeffnen.setEnabled(true);	
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowClosed(WindowEvent arg0) {}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}

}
