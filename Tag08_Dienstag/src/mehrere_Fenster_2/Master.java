package mehrere_Fenster_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

/*
	In diesem Experiment ("package mehrere_Fenster_2") soll gezeigt
	werden, wie sich der Master dem Slave zu erkenne gibt, so dass
	der Slave dem Master etwas senden kann.
*/

public class Master implements ActionListener {

	JFrame frmMaster;
	
	 
	JButton btnOeffnen;

	// Konstruktor:
	public Master() {
		createFrame();
		createControls();
		frmMaster.setVisible(true);
	}

	private void createFrame() {
		frmMaster = new JFrame();
		frmMaster.setSize(300, 300);
		frmMaster.setLocationRelativeTo(null);
		frmMaster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMaster.setLayout(null); // ermöglicht x-y-Platzierung von Controls
		frmMaster.setTitle("Master");
	}

	public void createControls() {
		btnOeffnen = new JButton();
		btnOeffnen.setBounds(10, 10, 260, 30);
		btnOeffnen.setText("noch ein Frame");
		btnOeffnen.addActionListener(this);
		frmMaster.add(btnOeffnen);

	}

	// Das ist der Event Handler des ActionListeners:
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		// 'this' meint: die eigene Klasse
		Slave frmSlave = new Slave(this);
		frmSlave.setVisible(true);
		
		btnOeffnen.setEnabled(false);
	}

	public static void main(String[] args) {
		new Master();
	}

}
