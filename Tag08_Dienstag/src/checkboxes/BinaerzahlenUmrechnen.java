package checkboxes;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import meinzeug.MyFrame;

public class BinaerzahlenUmrechnen implements ItemListener {

	// Globale Variablen:
	JCheckBox cb;
	JLabel lbl;
	MyFrame f;

	// Konstruktor:
	public BinaerzahlenUmrechnen() {

		f = new MyFrame(); // ... die Überladung ohne Parameter
		createControls();
		f.setVisible(true);
	}

	private void createControls() {

		cb = new JCheckBox();
		cb.setBounds(10, 10, 100, 30);
		cb.setText("cb");
		cb.addItemListener(this);
		f.add(cb);

		lbl = new JLabel();
		lbl.setBounds(150, 10, 100, 30);
		lbl.setText("aus");
		f.add(lbl);
	}

	public static void main(String[] args) {
		new BinaerzahlenUmrechnen();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		
		if (cb.isSelected()) {
			lbl.setText("an");
		} else {
			lbl.setText("aus");
		}
	}

}
