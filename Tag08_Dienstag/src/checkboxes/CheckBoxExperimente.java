package checkboxes;

import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import meinzeug.MyFrame;

public class CheckBoxExperimente implements ItemListener {

	// Globale Variablen:
	JCheckBox[] cb = new JCheckBox[8];
	JLabel lblDezimalzahl;
	MyFrame f;

	// Konstruktor:
	public CheckBoxExperimente() {

		f = new MyFrame(250, 150, "bin --> dez");
		createControls();
		f.setVisible(true);
	}

	private void createControls() {

		for (int i = 0; i < cb.length; i++) {
			cb[i] = new JCheckBox();
			cb[i].setBounds(190 - (i * 24), 10, 25, 25);
			cb[i].addItemListener(this);
			f.add(cb[i]);
		}

		lblDezimalzahl = new JLabel();
		lblDezimalzahl.setBounds(70, 50, 100, 50);
		lblDezimalzahl.setFont(new Font("Arial", Font.PLAIN, 50));
		lblDezimalzahl.setText("0");
		f.add(lblDezimalzahl);
	}

	@Override
	public void itemStateChanged(ItemEvent e) {

		int dezimalzahl = 0;
		
		if (cb[0].isSelected())	dezimalzahl += 1;
		if (cb[1].isSelected())	dezimalzahl += 2;
		if (cb[2].isSelected())	dezimalzahl += 4;
		if (cb[3].isSelected())	dezimalzahl += 8;
		if (cb[4].isSelected())	dezimalzahl += 16;
		if (cb[5].isSelected())	dezimalzahl += 32;
		if (cb[6].isSelected())	dezimalzahl += 64;
		if (cb[7].isSelected())	dezimalzahl += 128;
		
		lblDezimalzahl.setText("" + dezimalzahl);
	}

	public static void main(String[] args) {
		new CheckBoxExperimente();
	}

}

		// for (int i = 0; i < 8; i++) {
		//	  if (cb[i].isSelected()) dezimalzahl += Math.pow(2, i);
		// }
