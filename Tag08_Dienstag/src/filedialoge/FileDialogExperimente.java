package filedialoge;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import meinzeug.MyFrame;

/*
	In dieser Klasse sollen drei Dialoge gezeigt werden:
	- OpenFileDialog
	- SaveFileDialog
	- FileDialog (der �bergeordnete)
*/

public class FileDialogExperimente implements ActionListener {

	MyFrame f;
	JButton btn1, btn2, btn3;

	// Konstruktor
	public FileDialogExperimente() {

		f = new MyFrame(500, 170, "FileDialoge");

		createButtons();

		f.setVisible(true);

	}

	private void createButtons() {

		btn1 = new JButton("OpenDialog");
		btn1.setBounds(10, 10, 465, 30);
		btn1.addActionListener(this);
		f.add(btn1);

		btn2 = new JButton("SaveDialog");
		btn2.setBounds(10, 50, 465, 30);
		btn2.addActionListener(this);
		f.add(btn2);

		btn3 = new JButton("�bergeordneter Dialog");
		btn3.setBounds(10, 90, 465, 30);
		btn3.addActionListener(this);
		f.add(btn3);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// Instanziierung
		JFileChooser fc = new JFileChooser();

		if (e.getSource() == btn1) {

			// optional: Anfangsdirectory:
			fc.setCurrentDirectory(new File("C:\\Users\\Platz 6700\\Desktop"));

			// �ffnet den Open-File-Dialog
			fc.showOpenDialog(null);

			if (fc.getSelectedFile() != null) {
				// Name bzw. Pfad des Geklickten ermitteln und ausgeben:
				// String name = fc.getSelectedFile().getName(); f.setTitle(name);
				String absPathAndName = fc.getSelectedFile().getAbsoluteFile().toString();
				f.setTitle(absPathAndName);
			}
		}

		if (e.getSource() == btn2) {

			// optional: Anfangsdirectory:
			fc.setCurrentDirectory(new File("C:\\Users\\Platz 6700\\Desktop"));

			// �ffnet den Save-File-Dialog
			fc.showSaveDialog(null);

			if (fc.getSelectedFile() != null) {
				// Name bzw. Pfad des Geklickten ermitteln und ausgeben:
				// String name = fc.getSelectedFile().getName(); f.setTitle(name);
				String absPathAndName = fc.getSelectedFile().getAbsoluteFile().toString();
				f.setTitle(absPathAndName);
			}
		}

		if (e.getSource() == btn3) {
			
			// optional: Anfangsdirectory:
			fc.setCurrentDirectory(new File("C:\\Users\\Platz 6700\\Desktop"));

			// �ffnet den Save-File-Dialog
			fc.showDialog(null, "test"); // "test" ist Rand- und Buttonbeschriftung 

			if (fc.getSelectedFile() != null) {
				// Name bzw. Pfad des Geklickten ermitteln und ausgeben:
				// String name = fc.getSelectedFile().getName(); f.setTitle(name);
				String absPathAndName = fc.getSelectedFile().getAbsoluteFile().toString();
				f.setTitle(absPathAndName);
			}
		}

	}

	public static void main(String[] args) {
		new FileDialogExperimente();
	}

}
