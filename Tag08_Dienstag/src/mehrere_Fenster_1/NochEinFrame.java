package mehrere_Fenster_1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;


public class NochEinFrame implements ActionListener {

	JFrame frmMaster;
	Slave frmSlave = new Slave();
	JButton btnOeffnen;

	// Konstruktor:
	public NochEinFrame() {

		createFrame();
		createControls();
		frmMaster.setVisible(true);
	}

	private void createFrame() {
		frmMaster = new JFrame();
		frmMaster.setSize(300, 300);
		frmMaster.setLocationRelativeTo(null);
		frmMaster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMaster.setLayout(null); // ermöglicht x-y-Platzierung von Controls
		frmMaster.setTitle("Master");
	}

	private void createControls() {
		btnOeffnen = new JButton();
		btnOeffnen.setBounds(10, 10, 260, 30);
		btnOeffnen.setText("noch ein Frame");
		btnOeffnen.addActionListener(this);
		frmMaster.add(btnOeffnen);

	}

	// Das ist der Event Handler des ActionListeners:
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		frmSlave.setVisible(true);
	}

	public static void main(String[] args) {
		new NochEinFrame();
	}

}
