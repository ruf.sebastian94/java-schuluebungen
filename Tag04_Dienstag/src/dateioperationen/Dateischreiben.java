package dateioperationen;

import java.io.FileWriter;
import java.io.IOException;

public class Dateischreiben {

	// Konstruktor:
	public Dateischreiben() {
		
		try {
			
			FileWriter fw = new FileWriter("C:\\Users\\Platz 6700\\Desktop\\test.txt");
			
			fw.write("Das ist eine geschriebene Zeile.\r\n");
			fw.write("Das ist eine geschriebene Zeile.\r\n");
			// Wozu \r UND \n ? In manchen Betriebssystemen erforderlich.
			fw.close();
			
		} catch (IOException e) {
			
			System.out.println("Es ist ein Fehler beim Dateischreiben aufgetreten.");
		}
		
	}

	public static void main(String[] args) {
		new Dateischreiben(); // Aufruf des eigenen Konstruktors 
	}
}
