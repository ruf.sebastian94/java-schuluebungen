package dateioperationen;

import java.io.FileWriter;
import java.io.IOException;

public class Dateischreiben_Ranhaengen {

	// Konstruktor:
	public Dateischreiben_Ranhaengen() {
		
		try {
			
			// Man kann an vorhandene Dateien neuen Text ranhängen: "true"
			// Man kann vorhandene Dateien überschreiben: "false" oder 'nichts'
			FileWriter fw = new FileWriter("C:\\Users\\Platz 6700\\Desktop\\test.txt", false);
			
			fw.write("Das ist eine Zeile.\r\n");
			
			
			fw.close();
			
		} catch (IOException e) {
			
			System.out.println("Es ist ein Fehler beim Dateischreiben aufgetreten.");
		}
		
	}

	public static void main(String[] args) {
		new Dateischreiben_Ranhaengen(); // Aufruf des eigenen Konstruktors 
	}
}
