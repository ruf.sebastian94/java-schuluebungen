package dateioperationen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/*
	Wie man Dateien und Ordner anlegt, 
	l�scht sowie Dateien ausliest.
*/

public class Allgemeine_Dateioperationen {

	// Bereich f�r globale Variablen
	String pfadZumDesktop = "C:\\Users\\Platz 6700\\Desktop";

	// Konstruktor
	public Allgemeine_Dateioperationen() {

		try {

			// File f = new File(pfadZumDesktop + "\\susi.txt");

			// Eine Datei "susi.txt" auf dem Desktop erzeugen:
			// f.createNewFile();

			// Die Datei vom "susi.txt" vom Desktop l�schen:
			// f.delete();

			// Einen Ordner bzw. ein Vereichnis auf dem Desktop anlegen:
			// File pfad = new File(pfadZumDesktop + "\\Neuer Ordner");
			// pfad.mkdir();

			// Den Ordner "Neuer Ordner" vom Desktop l�schen:
			// File pfad = new File(pfadZumDesktop + "\\Neuer Ordner");
			// pfad.delete(); // Ordner muss leer sein!
			
			
			// Welche Elemente befinden sich im Ordner "Testordner"?
			File[] inhalt = new File(pfadZumDesktop + "\\Testordner").listFiles();
			
			// Alle Elemente ausgeben:
			// for (File foo : inhalt) System.out.println(foo.getName()); 
				
			// nur Ordner ausgeben:
			// for (File foo : inhalt) if(foo.isDirectory()) System.out.println("Ordner: " +foo.getName());
			
			// System.out.println("-------");
			
			// nur Dateien ausgeben:
			// for (File foo : inhalt) if(foo.isFile()) System.out.println("Datei: " + foo.getName());
				
			
			
			// Die Datei "test.txt" zeilenweise auslesen:
			FileReader fr = new FileReader(pfadZumDesktop + "\\test.txt");
			BufferedReader br = new BufferedReader(fr);
			
			String zeile = "";
			
			while((zeile = br.readLine()) != null) System.out.println(zeile);
			
			br.close(); fr.close(); // bitte nicht vergessen!
			
			
			
			
			
			
			
		} catch (Exception e) {

			System.out.println("Es lief was schief.");
		}

	}

	public static void main(String[] args) {

		// Wir rufen den eigenen Konstruktor auf, damit
		// wir ab sofort nicht-statische Methoden und Variablen
		// benutzen k�nnen:
		new Allgemeine_Dateioperationen();
	}

}
