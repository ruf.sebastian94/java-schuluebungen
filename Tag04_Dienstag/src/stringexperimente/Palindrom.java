package stringexperimente;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Palindrom implements ActionListener {

	// Bereich f�r globale Variablen:
	JFrame frm;
	JTextField txtEingabe;
	JButton btnVerarbeitung;
	JLabel lblAusgabe;

	// Konstruktor
	public Palindrom() {

		createFrame();
		createControls();

		frm.setVisible(true);
	}

	private void createFrame() {
		frm = new JFrame();
		frm.setBounds(500, 300, 400, 200);
		frm.setLayout(null);
		frm.setTitle("Palindromtest");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void createControls() {

		txtEingabe = new JTextField();
		txtEingabe.setBounds(10, 10, 300, 30);
		txtEingabe.setText("hier Wort eingeben");
		frm.add(txtEingabe);

		btnVerarbeitung = new JButton();
		btnVerarbeitung.setBounds(10, 50, 300, 30);
		btnVerarbeitung.setText("Test");
		btnVerarbeitung.addActionListener(this);
		frm.add(btnVerarbeitung);

		lblAusgabe = new JLabel();
		lblAusgabe.setBounds(10, 90, 300, 30);
		lblAusgabe.setFont(new Font("Arial", Font.PLAIN, 20));
		lblAusgabe.setText(". . .");
		frm.add(lblAusgabe);
	}

	private void palindromtest() {

		// Eingabe holen:
		String wort = txtEingabe.getText();
		wort = wort.toUpperCase();

		String trow = ""; // Wort - aber umgekehrt

		// trow zusammenbauen:
		for (int i = wort.length() - 1; i >= 0; i--) {
			trow += wort.charAt(i);
		}

		// Wort und Trow vergleichen:
		if (wort.equals(trow)) {
			lblAusgabe.setText(txtEingabe.getText() + " ist ein Palindrom.");
		} else {
			lblAusgabe.setText("Kein Palindrom.");
		}

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		palindromtest();
	}

	public static void main(String[] args) {
		new Palindrom();
	}

}
