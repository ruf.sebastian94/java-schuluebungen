package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt:
	
	- wie man mit JTable etc. ein CSV-Frontend realisieren kann
	
	- wie man die in SQL gebr�uchlichen Vorg�nge insert, 
	  update und delete realisieren kann
	   
*/

public class CSV_Frontend implements ActionListener, MouseListener, WindowListener {

	// Bereich f�r globale Variablen:
	MyFrame f = new MyFrame(600, 400, "CSV-Frontend");
	JTable table;
	JScrollPane sp;
	DefaultTableModel model;
	JButton btnClear;
	JLabel lblAusgabe;
	String kopfzeile = "";
	int spaltenzahl = -1;
	JTextField[] txt = new JTextField[4];
	JLabel[] lbl = new JLabel[4];
	JButton[] btn = new JButton[4];

	// Konstruktor:
	public CSV_Frontend() {

		holeKopfzeile();
		createTable();
		createControls();
		fillTable();

		f.addWindowListener(this);
		f.setVisible(true);
	}

	private void holeKopfzeile() {

		try {

			FileReader fr = new FileReader("Kunden.txt");
			BufferedReader br = new BufferedReader(fr);
			kopfzeile = br.readLine();
			br.close();
			fr.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Fehler beim Holen der Kopfzeile");
		}
	}

	private void createTable() {

		// 1. aus der Kopfzeile die Spalten generieren:
		String[] columns = kopfzeile.split(";");

		spaltenzahl = columns.length; // Bei uns: 4

		for (int i = 0; i < spaltenzahl; i++) {
			columns[i] = columns[i].trim();
		}

		Object[][] data = new Object[0][spaltenzahl]; // Zeile, Spalte

		model = new DefaultTableModel(data, columns);
		table = new JTable(model);
		table.addMouseListener(this);

		sp = new JScrollPane(table);
		sp.setBounds(10, 10, 560, 200);

		f.add(sp);
	}

	private void createControls() {

		// TextFields & Buttons sowie deren Beschriftungen:
		for (int i = 0; i < txt.length; i++) {

			// Labels:
			String[] textFieldBeschriftungen = { "ID:", "Vorname:", "Nachname:", "Telefon:" };
			lbl[i] = new JLabel(textFieldBeschriftungen[i]);
			lbl[i].setBounds(10, 220 + i * 30, 100, 30);
			f.add(lbl[i]);

			// TextFields:
			txt[i] = new JTextField();
			txt[i].setBounds(90, 220 + i * 30, 150, 28);
			f.add(txt[i]);

			// Buttons:
			String[] buttonBeschriftungen = { "clear", "insert", "update", "delete" };
			btn[i] = new JButton(buttonBeschriftungen[i]);
			btn[i].setBounds(480, 220 + i * 30, 90, 28);
			btn[i].addActionListener(this);
			f.add(btn[i]);

		}

//		btnClear = new JButton("neue Zeile");
//		btnClear.setBounds(10, 220, 100, 30);
//		btnClear.addActionListener(this);
//		f.add(btnClear);
//
//		lblAusgabe = new JLabel(". . .");
//		lblAusgabe.setBounds(130, 220, 400, 30);
//		f.add(lblAusgabe);
	}

	private void fillTable() {

		try {

			FileReader fr = new FileReader("Kunden.txt");
			BufferedReader br = new BufferedReader(fr);

			String zeile = br.readLine(); // Kopfzeile lesen und ignorieren

			while (zeile != null) // solange die aktuelle Zeile Daten enth�lt
			{

				zeile = br.readLine(); // Zeile aus der Datei holen

				if (zeile != null) {

					String[] data = zeile.split(";"); // Zeile in Einzelwerte splitten

					// Leerzeichen entfernen:
					for (int i = 0; i < data.length; i++)
						data[i] = data[i].trim();

					// Nur Zeilen in die JTable schreiben, die nicht leer sind
					if (data[0].length() > 0) { // Ist eine ID vorhanden?
						model.addRow(data); // Zeile in
					}
				}
			}

			br.close();
			fr.close();

		} catch (Exception e) {

			JOptionPane.showMessageDialog(null, "Fehler beim Dateilesen");
		}
	}

	private void clearTextFieldsAndSelection() {
		for (int i = 0; i < txt.length; i++) {
			txt[i].setText("");
			table.clearSelection();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btn[0]) // btn[0] ist btnClear
		{
			clearTextFieldsAndSelection();
		}

		if (e.getSource() == btn[1]) // btn[1] ist btnInsert
		{
			model.addRow(new String[] { txt[0].getText(), txt[1].getText(), txt[2].getText(), txt[3].getText() });

			clearTextFieldsAndSelection();
		}

		if (e.getSource() == btn[2]) // btn[2] ist btnUpdate
		{
			for (int i = 0; i < txt.length; i++) {// txt.length ist die Anzahl der TextFields
				model.setValueAt(txt[i].getText(), table.getSelectedRow(), i);
			}
		}

		if (e.getSource() == btn[3]) // btn[3] ist btnDelete
		{
			if (table.getSelectedRow() > -1)
				model.removeRow(table.getSelectedRow());
			clearTextFieldsAndSelection();
		}

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// Das ist ein Event Handler des MouseListeners:

		int zeile = table.getSelectedRow(); // Welche Zeile wurde geklickt?

		// Die Daten der angeklickten Tabellenzeile in den TextFields ausgeben:
		for (int spalte = 0; spalte < spaltenzahl; spalte++) {
			txt[spalte].setText(model.getValueAt(zeile, spalte).toString());
		}
	}

	@Override
	public void windowClosing(WindowEvent arg0) {

		// dieser event handler wird ausgef�hrt,
		// wenn sich das JFrame f schlie�t

		try {

			FileWriter fw = new FileWriter("Kunden.txt");
			fw.write(kopfzeile + "\n");

			for (int zeile = 0; zeile < model.getRowCount(); zeile++) {
				
				String datenzeile = ""; 
				
				for (int spalte = 0; spalte < model.getColumnCount(); spalte++) {
					datenzeile += model.getValueAt(zeile, spalte).toString() + "; ";
				}
				
				// Das letzte Semikolon und das letzte Leerzeichen
				// aus der zu schreibende Zeile entfernen:
				datenzeile = datenzeile.substring(0, datenzeile.lastIndexOf(";")-2);
				
				// Die Zeile 
				fw.write(datenzeile + "\n");
			}

			fw.close();

		} catch (Exception e) {

			System.out.println("Fehler beim Dateischreiben!");

		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
	}

	public static void main(String[] args) {
		new CSV_Frontend();
	}
}
