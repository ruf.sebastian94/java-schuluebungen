package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt, wie man eine 
	JTable f�r Ausgaben benutzt.
	
	- echte Spaltenk�pfen
	- wir k�nnen nachtr�glich die Daten �ndern 
*/

public class JTable3 implements ActionListener {

	// Bereich f�r globale Variablen:
	MyFrame f = new MyFrame(600, 300, "JTable 3");
	JTable table;
	JScrollPane sp;
	DefaultTableModel model;
	JButton btnNeueZeile; 
	
	// Konstruktor
	public JTable3() {

		createTable();
		createControls();

		f.setVisible(true);
	}

	private void createTable() {

		String[] columns = { "Spalte 1", "Spalte 2", "Spalte 3", "Spalte 4" };

		Object[][] data = { { "a", "b", "c", "d" }, { "e", "f", "g", "h" }, { "i", "j", "k", "l" } };

		model = new DefaultTableModel(data, columns);
		table = new JTable(model);

		sp = new JScrollPane(); // entweder: JScrollPane(table)
		sp.setBounds(10, 10, 560, 100);
		sp.setViewportView(table); // oder: setViewportView(table)

		f.add(sp);
	}

	private void createControls() {
		btnNeueZeile = new JButton("neue Zeile");
		btnNeueZeile.setBounds(10, 220, 100, 30);
		btnNeueZeile.addActionListener(this);
		f.add(btnNeueZeile);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		// Es sollen bei Buttonbet�tigung 4 ganze Zufallszahlen [1, 100] 
		// als neue Zeile hinzugef�gt werden

		Random r = new Random();
		model.addRow(new Object[] {r.nextInt(100)+1, r.nextInt(100)+1, r.nextInt(100)+1, r.nextInt(100)+1 });

		// Eine Zelle auslesen, hier Zelle: Zeile 0, Spalte 0 
		String zelleninhalt = model.getValueAt(0, 0).toString(); // "a"
		System.out.println(zelleninhalt);
		
		// den Wert einer Tabellenzelle setzen
		model.setValueAt("Mittagspause", 1, 1); // gerne mit Schleifen
		
		// ganze Tabelle leeren (alle Datenzeilen l�schen)
		// model.setRowCount(0);		
	}

	public static void main(String[] args) {
		new JTable3();
	}

}
