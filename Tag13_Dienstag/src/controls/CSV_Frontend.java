package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import java.io.*;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt:
	
	- wie man mit JTable etc. ein CSV-Frontend realisieren kann
	
	- wie man (sp�ter) die in SQL gebr�uchlichen Vorg�nge insert, 
	  update und delete realisieren kann
	   
*/

public class CSV_Frontend implements ActionListener, MouseListener {

	// Bereich f�r globale Variablen:
	MyFrame f = new MyFrame(600, 400, "CSV-Frontend");
	JTable table;
	JScrollPane sp;
	DefaultTableModel model;
	JButton btnClear;
	JLabel lblAusgabe;
	String kopfzeile = "";
	int spaltenzahl = -1;

	// Konstruktor:
	public CSV_Frontend() {

		holeKopfzeile();
		createTable();
		// createControls();
		fillTable();

		f.setVisible(true);
	}

	private void holeKopfzeile() {

		try {

			FileReader fr = new FileReader("Kunden.txt");
			BufferedReader br = new BufferedReader(fr);
			kopfzeile = br.readLine();
			br.close();
			fr.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Fehler beim Holen der Kopfzeile");
		}
	}

	private void createTable() {

		// 1. aus der Kopfzeile die Spalten generieren:
		String[] columns = kopfzeile.split(";");

		spaltenzahl = columns.length; // Bei uns: 4
		
		for (int i = 0; i < spaltenzahl; i++) {
			columns[i] = columns[i].trim();
		}

		Object[][] data = new Object[0][spaltenzahl]; // Zeile, Spalte

		model = new DefaultTableModel(data, columns);
		table = new JTable(model);
		table.addMouseListener(this);

		sp = new JScrollPane(table); 
		sp.setBounds(10, 10, 560, 200);
	
		f.add(sp);
	}

	private void createControls() {

		btnClear = new JButton("neue Zeile");
		btnClear.setBounds(10, 220, 100, 30);
		btnClear.addActionListener(this);
		f.add(btnClear);

		lblAusgabe = new JLabel(". . .");
		lblAusgabe.setBounds(130, 220, 400, 30);
		f.add(lblAusgabe);
	}

	private void fillTable() {

		try {

			FileReader fr = new FileReader("Kunden.txt");
			BufferedReader br = new BufferedReader(fr);

			String zeile = br.readLine(); // Kopfzeile lesen und ignorieren

			while (zeile != null) // solange die aktuelle Zeile Daten enth�lt 
			{
				
				zeile = br.readLine(); // Zeile aus der Datei holen
								
				if (zeile != null) {
					
					String[] data = zeile.split(";"); // Zeile in Einzelwerte splitten
					
					// Leerzeichen entfernen: 
					for (int i = 0; i < data.length; i++) data[i] = data[i].trim();
					
					if(data[0].length()>0) { // Ist eine ID vorhanden?
						model.addRow(data);  // Zeile in 
					}
				}
			}

			br.close();
			fr.close();

		} catch (Exception e) {

			JOptionPane.showMessageDialog(null, "Fehler beim Dateilesen");

		}

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

	}

	public static void main(String[] args) {
		new CSV_Frontend();
	}

	@Override
	public void mousePressed(MouseEvent e) {

		// int spalte = table.getSelectedColumn(); // siehe Schleife!
		int zeile = table.getSelectedRow();

		String strAusgabe = "";

		for (int spalte = 0; spalte < spaltenzahl; spalte++) {
			strAusgabe += model.getValueAt(zeile, spalte).toString() + "   ";
		}

		lblAusgabe.setText(strAusgabe);

	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
