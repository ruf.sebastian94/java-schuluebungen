package controls;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt, wie man eine einfache
	JTable f�r einfache Ausgaben benutzt.
	
	- Nur-Ausgabe mit ECHTEN Spaltenk�pfen
	- wir k�nnen nachtr�glich keine Daten �ndern 
*/


public class JTable2 {

	MyFrame f = new MyFrame(600, 300, "JTable 2");
	
	// Konstruktor
	public JTable2() {
		
		createTable();
		
		f.setVisible(true);
	}

	private void createTable() {
		
		String[] columns = {"Spalte 1", "Spalte 2", "Spalte 3", "Spalte 4"};
		
		
		Object[][] data = {	{"a", "b", "c", "d"},
							{"e", "f", "g", "h"},
							{"i", "j", "k", "l"} }; 
		
		JTable table = new JTable(data, columns);		
		
		JScrollPane sp = new JScrollPane(); // entweder: JScrollPane(table)
		sp.setBounds(10, 10, 560, 200);
		sp.setViewportView(table);          // oder: setViewportView(table)
		
		f.add(sp);
	}

	public static void main(String[] args) {
		new JTable2();
	}

}
