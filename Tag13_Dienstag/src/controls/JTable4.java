package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt:
	
	- Ausgabe von Daten in der JTable
	- Anklicken und Weiterverarbeiten von Daten	 
*/

public class JTable4 implements ActionListener, MouseListener {

	// Bereich f�r globale Variablen:
	MyFrame f = new MyFrame(600, 300, "JTable 4");
	JTable table;
	JScrollPane sp;
	DefaultTableModel model;
	JButton btnClear;
	JLabel lblAusgabe;
	
	// Konstruktor
	public JTable4() {

		createTable();
		createControls();

		f.setVisible(true);
	}

	private void createTable() {

		String[] columns = { "Spalte 1", "Spalte 2", "Spalte 3", "Spalte 4" };

		Object[][] data = { { "a", "b", "c", "d" }, { "e", "f", "g", "h" }, { "i", "j", "k", "l" } };

		model = new DefaultTableModel(data, columns);
		table = new JTable(model);
		table.addMouseListener(this);
		
		//table.setCellSelectionEnabled(true); // nur die geklickte Zelle einf�rben9
		
		
		sp = new JScrollPane(); // entweder: JScrollPane(table)
		sp.setBounds(10, 10, 560, 100);
		sp.setViewportView(table); // oder: setViewportView(table)

		f.add(sp);
	}

	private void createControls() {
		btnClear = new JButton("neue Zeile");
		btnClear.setBounds(10, 220, 100, 30);
		btnClear.addActionListener(this);
		f.add(btnClear);
		
		lblAusgabe = new JLabel(". . .");
		lblAusgabe.setBounds(130, 220, 400, 30);
		f.add(lblAusgabe);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		// Es sollen bei Buttonbet�tigung 4 ganze Zufallszahlen [1, 100] 
		// als neue Zeile hinzugef�gt werden

		Random r = new Random();
		model.addRow(new Object[] {r.nextInt(100)+1, r.nextInt(100)+1, r.nextInt(100)+1, r.nextInt(100)+1 });

		// Eine Zelle auslesen, hier Zelle: Zeile 0, Spalte 0 
		String zelleninhalt = model.getValueAt(0, 0).toString(); // "a"
		System.out.println(zelleninhalt);
		
		// den Wert einer Tabellenzelle setzen
		model.setValueAt("Mittagspause", 1, 1); // gerne mit Schleifen
		
		// ganze Tabelle leeren (alle Datenzeilen l�schen)
		// model.setRowCount(0);		
	}

	public static void main(String[] args) {
		new JTable4();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void mouseEntered(MouseEvent e) {
				
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		// int spalte = table.getSelectedColumn(); // siehe Schleife!
		int zeile = table.getSelectedRow();
		
		String strAusgabe = "";
		
		for (int spalte = 0; spalte < 4; spalte++) {
			strAusgabe += model.getValueAt(zeile, spalte).toString() + "   ";
		}
		
		lblAusgabe.setText(strAusgabe);
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
