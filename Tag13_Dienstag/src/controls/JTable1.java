package controls;

import javax.swing.JTable;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt, wie man eine einfachste
	JTable f�r einfache Ausgaben benutzt.
	
	- Anwendung: Nur-Ausgabe 
	- wir haben keine ECHTEN Spaltenk�pfe
	- wir k�nnen nachtr�glich keine Daten �ndern 
*/


public class JTable1 {

	MyFrame f = new MyFrame(600, 300, "JTable 1");
	
	// Konstruktor
	public JTable1() {
		
		createTable();
		
		f.setVisible(true);
	}

	private void createTable() {
		
		String[] columns = {"egal", "was", "hier", "steht" };
		
		
		Object[][] data = {	/*{"Spalte 1", "Spalte 2", "Spalte 3", "Spalte 4"},*/
				            {"a", "b", "c", "d"},
							{"e", "f", "g", "h"},
							{"i", "j", "k", "l"} }; 
		
		JTable table = new JTable(data, columns);		
		
		table.setBounds(10, 10, 560, 250);
		
		f.add(table);
	}

	public static void main(String[] args) {
		new JTable1();
	}

}
