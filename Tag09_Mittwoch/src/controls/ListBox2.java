package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt, wie man eine einfachste
	JList erzeugt. 
	
	Sie kann nur eine Sammlung von Elementen ausgeben.
	Die Elemente kann man anklicken und so weiterverarbeiten. 
*/

public class ListBox2 implements ListSelectionListener, ActionListener {

	MyFrame f = new MyFrame(720, 500, "ListBox - JList");
	JList<String> lst;
	JLabel lbl;
	JTextField txtAnsEndeHinzufuegen;
	JButton btnAnsEndeHinzufuegen, btnAlleElementeLoeschen, btnListeSpeichern;
	DefaultListModel<String> model;

	// Konstruktor
	public ListBox2() {

		model = new DefaultListModel<String>();

		lst = new JList<String>(model);
		lst.setBounds(10, 10, 250, 320);
		lst.addListSelectionListener(this);
		f.add(lst);

		txtAnsEndeHinzufuegen = new JTextField();
		txtAnsEndeHinzufuegen.setBounds(270, 10, 200, 30);
		f.add(txtAnsEndeHinzufuegen);

		btnAnsEndeHinzufuegen = new JButton("an das Ende hinzuf�gen");
		btnAnsEndeHinzufuegen.setBounds(480, 10, 200, 30);
		btnAnsEndeHinzufuegen.addActionListener(this);
		f.add(btnAnsEndeHinzufuegen);
		
		btnAlleElementeLoeschen = new JButton("Alle Elemente l�schen");
		btnAlleElementeLoeschen.setBounds(270, 50, 410, 30);
		btnAlleElementeLoeschen.addActionListener(this);
		f.add(btnAlleElementeLoeschen);
		
		lbl = new JLabel("Index: -1");
		lbl.setBounds(10, 370, 250, 30);
		f.add(lbl);

		f.setVisible(true);

		// nachdem das Frame sichtbar wurde, f�ge ich dem Model nun Element hinzu:

		// 1. Einzelne Elemente an das Ende der Liste hinzuf�gen:
		model.addElement("Birne");
		model.addElement("Himbeere");
		model.addElement("Ananas");

		// 2. Einen kompletten Sammelbeh�lter (hier: fruechte) hinzuf�gen:
		String[] fruechte = { "Apfel", "Orange", "Erdbeere", "Banane", "Pfirsich", "Pflaume" };
		for (int i = 0; i < fruechte.length; i++)
			model.addElement(fruechte[i]);

		// 3. Einzelnes Element an bestimmter Stelle der Liste hinzuf�gen:
		// An der Stelle befindliche Element rutschen weiter.
		model.add(5, "Braunbeere");

		// 4. Element von bestimmter Position (Index) l�schen:
		model.removeElementAt(0); // Birne
		model.remove(1); // Ananas

		// remove() gibt das gel�schte Element zur�ck
		String foo = model.remove(4);
		// JOptionPane.showMessageDialog(null, foo); // Erdbeere

	}

	// event handler des ListSelectionListeners:
	@Override
	public void valueChanged(ListSelectionEvent e) {

		// Welches Listenelement wurde geklickt? (Index und Wert)
		int index = lst.getSelectedIndex();
		String element = lst.getSelectedValue().toString();

		lbl.setText("Index: " + index + "       Element: " + element);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == btnAnsEndeHinzufuegen) {
			model.addElement(txtAnsEndeHinzufuegen.getText());
			txtAnsEndeHinzufuegen.setText("");
		}
		
		if (e.getSource() == btnAlleElementeLoeschen) {
			model.clear();
		}
		
	}

	public static void main(String[] args) {
		new ListBox2();
	}

}
