package controls;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import meinzeug.MyFrame;

public class RadioButtons2 implements ItemListener {

	// Bereich für globale Variablen:
	MyFrame f = new MyFrame(580, 360,"RadioButtonÜbung");
	JRadioButton[] rb = new JRadioButton[10];
	JLabel lbl;
	
	
	// Konstruktor:
	public RadioButtons2() {
		
		// Label zur Ausgabe
		lbl = new JLabel();
		lbl.setBounds(100, 250, 400, 50);
		lbl.setFont(new Font("Arial", Font.PLAIN, 30));
		f.add(lbl);
		
		// ButtonGroups:
		ButtonGroup groupGeschlecht = new ButtonGroup();
		ButtonGroup groupNationalitaet = new ButtonGroup();
		ButtonGroup groupBlutgruppe = new ButtonGroup();
		
		// Panels
		JPanel panGeschlecht = new JPanel();
		panGeschlecht.setBounds(10, 10, 130, 170);
		panGeschlecht.setLayout(null);
		panGeschlecht.setBorder(BorderFactory.createTitledBorder("Geschlecht"));
		f.add(panGeschlecht);
		
		JPanel panNationalitaet = new JPanel();
		panNationalitaet.setBounds(210, 10, 130, 170);
		panNationalitaet.setLayout(null);
		panNationalitaet.setBorder(BorderFactory.createTitledBorder("Nationalitaet"));
		f.add(panNationalitaet);
		
		JPanel panBlutgruppe = new JPanel();
		panBlutgruppe.setBounds(410, 10, 130, 170);
		panBlutgruppe.setLayout(null);
		panBlutgruppe.setBorder(BorderFactory.createTitledBorder("Blutgruppe"));
		f.add(panBlutgruppe);
		
		String[] beschriftung = {"m", "w", "d", "deutsch", "iranisch", "russisch", "A", "B", "A/B", "0"};
		
		// Geschlecht-RadioButtons erzeugen:
		for (int i = 0; i <= 2; i++) {
			rb[i] = new JRadioButton(beschriftung[i]);
			rb[i].setBounds(10, 30 + i*30, 100, 30);
			rb[i].addItemListener(this);
			groupGeschlecht.add(rb[i]);
			panGeschlecht.add(rb[i]);
		}
		
		// Nationalitäts-RadioButtons erzeugen:
		for (int i = 3; i <= 5; i++) {
			rb[i] = new JRadioButton(beschriftung[i]);
			rb[i].setBounds(10, i*30 - 60, 100, 30);
			rb[i].addItemListener(this);
			groupNationalitaet.add(rb[i]);
			panNationalitaet.add(rb[i]);
		}
		
		// Blutgruppen-RadioButtons erzeugen:
		for (int i = 6; i <= 9; i++) {
			rb[i] = new JRadioButton(beschriftung[i]);
			rb[i].setBounds(10, i*30 - 150, 100, 30);
			rb[i].addItemListener(this);
			groupBlutgruppe.add(rb[i]);
			panBlutgruppe.add(rb[i]);
		}
		
		// vorausgewählt:
		rb[0].setSelected(true);
		rb[3].setSelected(true);
		rb[6].setSelected(true);
			
		
		f.setVisible(true);
	}
	

	@Override
	public void itemStateChanged(ItemEvent arg0) {

		String ausgabe = "";
		
		if(rb[0].isSelected()) ausgabe += "männlich - "; 
		if(rb[1].isSelected()) ausgabe += "weiblich - ";
		if(rb[2].isSelected()) ausgabe += "divers - ";
			
		if(rb[3].isSelected()) ausgabe += "deutsch - "; 
		if(rb[4].isSelected()) ausgabe += "iranisch - ";
		if(rb[5].isSelected()) ausgabe += "russisch - ";
			
		if(rb[6].isSelected()) ausgabe += "A"; 
		if(rb[7].isSelected()) ausgabe += "B";
		if(rb[8].isSelected()) ausgabe += "AB";
		if(rb[9].isSelected()) ausgabe += "0";
		
		lbl.setText(ausgabe);
	}

	public static void main(String[] args) {
		new RadioButtons2();
	}
}
