package controls;

import javax.swing.JList;

import meinzeug.MyFrame;


/*
	In dieser Klasse wird gezeigt, wie man eine einfachste
	JList erzeugt. Sie kann nur eine Sammlung von Elementen ausgeben. 
	
*/ 

public class ListBox1 {

	MyFrame f = new MyFrame(300, 600, "ListBox - JList");
	
	
	// Konstruktor
	public ListBox1() {
		
		String[] fruechte = {"Apfel", "Orange", "Erdbeere", 
				             "Banane", "Pfirsich", "Pflaume"}; 
		
		JList lst = new JList(fruechte);
		lst.setBounds(10, 10, 250, 550);
		f.add(lst);
		
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new ListBox1();
	}

}
