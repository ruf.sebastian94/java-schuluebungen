package controls;

import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import meinzeug.MyFrame;

public class Radiobuttons1 implements ItemListener {

	// Globale Variablen
	MyFrame f = new MyFrame(300, 300, "Radiobuttons");
	JRadioButton rb1, rb2, rb3;
	JLabel lbl;

	public Radiobuttons1() {
		lbl = new JLabel();
		lbl.setText("1");
		lbl.setBounds(180, 30, 100, 50);
		lbl.setFont(new Font("Arial", Font.PLAIN, 40));
		f.add(lbl);

		f.setVisible(true);

		rb1 = new JRadioButton("rb1");
		rb1.setBounds(10, 10, 100, 30);
		rb1.setSelected(true);
		rb1.addItemListener(this);
		f.add(rb1);

		rb2 = new JRadioButton("rb2");
		rb2.setBounds(10, 40, 100, 30);
		rb2.addItemListener(this);
		f.add(rb2);

		rb3 = new JRadioButton("rb3");
		rb3.setBounds(10, 70, 100, 30);
		rb3.addItemListener(this);
		f.add(rb3);

		ButtonGroup group = new ButtonGroup(); // Buttons von einander abhängig machen
		group.add(rb1);
		group.add(rb2);
		group.add(rb3);

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (rb1.isSelected())
			lbl.setText("1");
		if (rb2.isSelected())
			lbl.setText("2");
		if (rb3.isSelected())
			lbl.setText("3");

	}

	public static void main(String[] args) {
		new Radiobuttons1();

	}

}
