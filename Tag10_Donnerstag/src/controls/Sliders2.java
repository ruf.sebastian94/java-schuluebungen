package controls;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import meinzeug.MyFrame; // aus dem Projekt "Tools"

/*

	In dieser Klasse soll zus�tzlich zu Sliders1 gezeigt werden,
	
	- wie man Minimum, Maximum und den aktuellen Wert setzen kann
	- wie man die Lage des Sliders ver�ndern kann
	- wie man eine Skale anbringt.

*/
public class Sliders2 implements ChangeListener {

	MyFrame f = new MyFrame(220, 600, "Slider");
	JSlider sli;
	JLabel lbl;

	public Sliders2() {

		sli = new JSlider(0, 255, 0); // Min, Max, Startwert
		sli.setOrientation(JSlider.VERTICAL);
		sli.setBounds(50, 10, 100, 450);
		sli.addChangeListener(this);
		
		// Skale
		sli.setMajorTickSpacing(10); // Gro�e Striche
		sli.setMinorTickSpacing(5);  // Kleine Striche
		sli.setPaintTicks(true);
		sli.setPaintLabels(true);
		
		
		
		
		f.add(sli);
		
		lbl = new JLabel("0");
		lbl.setBounds(90, 470, 50, 30);
		f.add(lbl);
		

		f.setVisible(true);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		int wert = sli.getValue();
		lbl.setText("" + wert);

	}

	public static void main(String[] args) {
		new Sliders2();
	}

}
