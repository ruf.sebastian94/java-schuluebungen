package controls;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import meinzeug.MyFrame; // aus dem Projekt "Tools"

/*

	In dieser Klasse soll mit Hilfe dreier Sliders die r-g-b-Farbe von ...
	eingestellt werden k�nnen.
	
	Color c = new Color(r, g, b);  // r, g, b = [0, 255]

*/
public class Sliders3 implements ChangeListener {

	MyFrame f = new MyFrame(400, 600, "Slider");
	JSlider sliRed, sliGreen, sliBlue;
	JLabel lblRed, lblGreen, lblBlue;
	Color c;

	public Sliders3() {

		
		
		sliRed = new JSlider(0, 255, 0); // Min, Max, Startwert
		sliRed.setOrientation(JSlider.VERTICAL);
		sliRed.setBounds(50, 10, 50, 450);
		sliRed.addChangeListener(this);
		f.add(sliRed);
		
		sliGreen = new JSlider(0, 255, 0); // Min, Max, Startwert
		sliGreen.setOrientation(JSlider.VERTICAL);
		sliGreen.setBounds(150, 10, 50, 450);
		sliGreen.addChangeListener(this);
		f.add(sliGreen);
		
		sliBlue = new JSlider(0, 255, 0); // Min, Max, Startwert
		sliBlue.setOrientation(JSlider.VERTICAL);
		sliBlue.setBounds(250, 10, 50, 450);
		sliBlue.addChangeListener(this);
		f.add(sliBlue);
		
		lblRed = new JLabel("0");
		lblRed.setBounds(70, 470, 50, 30);
		f.add(lblRed);
		
		lblGreen = new JLabel("0");
		lblGreen.setBounds(170, 470, 50, 30);
		f.add(lblGreen);
		
		lblBlue = new JLabel("0");
		lblBlue.setBounds(270, 470, 50, 30);
		f.add(lblBlue);
		
		c=Color.BLACK;
		f.getContentPane().setBackground(c);
		sliRed.setBackground(c);
		sliGreen.setBackground(c);
		sliBlue.setBackground(c);
		
		f.setVisible(true);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		
		int rot, gruen, blau;
		
		rot = sliRed.getValue();
		gruen = sliGreen.getValue();
		blau = sliBlue.getValue();
		
		lblRed.setText("" + rot);
		lblGreen.setText("" + gruen);
		lblBlue.setText("" + blau);
		
		c = new Color(rot, gruen, blau);
		f.getContentPane().setBackground(c);
		sliRed.setBackground(c);
		sliGreen.setBackground(c);
		sliBlue.setBackground(c);
		

	}

	public static void main(String[] args) {
		new Sliders3();
	}

}
