package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt, wie man eine komplexe
	JList erzeugt. 
	
	- Sie kann eine Sammlung von Elementen ausgeben.
	- Die Elemente kann man anklicken und so weiterverarbeiten.
	- Man kann Elemente hinzuf�gen und entfernen.
	- Man kann die Elemente in einer Datei im Projektordner speichern. 
*/

public class ListBox3 implements ListSelectionListener, ActionListener {

	MyFrame f = new MyFrame(720, 500, "ListBox - JList");
	JList<String> lst;
	JLabel lbl;
	JTextField txtAnsEndeHinzufuegen;
	JButton btnAnsEndeHinzufuegen, btnAlleElementeLoeschen, btnListeInDateiSpeichern;
	DefaultListModel<String> model;

	// Konstruktor
	public ListBox3() {

		model = new DefaultListModel<String>();

		lst = new JList<String>(model);
		lst.setBounds(10, 10, 250, 320);
		lst.addListSelectionListener(this);
		f.add(lst);

		txtAnsEndeHinzufuegen = new JTextField();
		txtAnsEndeHinzufuegen.setBounds(270, 10, 200, 30);
		f.add(txtAnsEndeHinzufuegen);

		btnAnsEndeHinzufuegen = new JButton("an das Ende hinzuf�gen");
		btnAnsEndeHinzufuegen.setBounds(480, 10, 200, 30);
		btnAnsEndeHinzufuegen.addActionListener(this);
		f.add(btnAnsEndeHinzufuegen);

		btnAlleElementeLoeschen = new JButton("Alle Elemente l�schen");
		btnAlleElementeLoeschen.setBounds(270, 50, 410, 30);
		btnAlleElementeLoeschen.addActionListener(this);
		f.add(btnAlleElementeLoeschen);

		btnListeInDateiSpeichern = new JButton("in Datei speichern");
		btnListeInDateiSpeichern.setBounds(270, 300, 410, 30);
		btnListeInDateiSpeichern.addActionListener(this);
		f.add(btnListeInDateiSpeichern);

		lbl = new JLabel("Index: -1");
		lbl.setBounds(10, 370, 250, 30);
		f.add(lbl);

		f.setVisible(true);

		// nachdem das Frame sichtbar wurde, f�ge ich dem Model nun Element hinzu:

		// 1. Einzelne Elemente an das Ende der Liste hinzuf�gen:
		model.addElement("Birne");
		model.addElement("Himbeere");
		model.addElement("Ananas");

		// 2. Einen kompletten Sammelbeh�lter (hier: fruechte) hinzuf�gen:
		String[] fruechte = { "Apfel", "Orange", "Erdbeere", "Banane", "Pfirsich", "Pflaume" };
		for (int i = 0; i < fruechte.length; i++)
			model.addElement(fruechte[i]);

		// 3. Einzelnes Element an bestimmter Stelle der Liste hinzuf�gen:
		// An der Stelle befindliche Element rutschen weiter.
		model.add(5, "Braunbeere");

		// 4. Element von bestimmter Position (Index) l�schen:
		model.removeElementAt(0); // Birne
		model.remove(1); // Ananas

		// remove() gibt das gel�schte Element zur�ck
		String foo = model.remove(4);
		// JOptionPane.showMessageDialog(null, foo); // Erdbeere

	}

	private void listeInDateiSpeichern() {

		try {
			FileWriter fw = new FileWriter("liste.txt");
			
			// jede Zeile aus der ListBox in die Datei "liste.txt" schreiben:	
			for (int i = 0; i < model.getSize(); i++) {
				fw.write(model.get(i) + "\n");
			}
			
			fw.close();
			
		} catch (IOException e) {
			System.out.println("Fehler beim Schreiben!");
		}
		
		JOptionPane.showMessageDialog(null, "Datei geschrieben");
	}

	// event handler des ListSelectionListeners:
	@Override
	public void valueChanged(ListSelectionEvent e) {

		// Welches Listenelement wurde geklickt? (Index und Wert)
		int index = lst.getSelectedIndex();
		String element = lst.getSelectedValue().toString();

		lbl.setText("Index: " + index + "       Element: " + element);
	}

	// event handler des ActionListeners:
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnAnsEndeHinzufuegen) {
			model.addElement(txtAnsEndeHinzufuegen.getText());
			txtAnsEndeHinzufuegen.setText("");
		}

		if (e.getSource() == btnAlleElementeLoeschen) {
			model.clear();
		}

		if (e.getSource() == btnListeInDateiSpeichern) {
			listeInDateiSpeichern(); // Methodenaufruf
		}

	}

	public static void main(String[] args) {
		new ListBox3();
	}

}
