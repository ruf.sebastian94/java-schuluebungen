package controls;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import meinzeug.MyFrame; // aus dem Projekt "Tools"

public class Sliders1 implements ChangeListener {

	MyFrame f = new MyFrame(520, 100, "Slider");
	JSlider sli;
	JLabel lbl;

	public Sliders1() {

		sli = new JSlider();
		sli.setBounds(10, 10, 450, 30);
		sli.addChangeListener(this);
		f.add(sli);
		
		lbl = new JLabel("50");
		lbl.setBounds(465, 10, 50, 30);
		f.add(lbl);
		

		f.setVisible(true);
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		int wert = sli.getValue();
		lbl.setText("" + wert);

	}

	public static void main(String[] args) {
		new Sliders1();
	}

}
