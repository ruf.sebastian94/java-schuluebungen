package controls;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import meinzeug.MyFrame;


/*
	In dieser Klasse wird gezeigt, wie man eine einfachste
	JList erzeugt. Sie kann nur eine Sammlung von Elementen ausgeben. 
	
*/ 

public class ComboBox1 {

	MyFrame f = new MyFrame(320, 600, "ComboBox - JComboBox");
	DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
	
	
	// Konstruktor
	public ComboBox1() {
		
		JComboBox<String> cbx = new JComboBox<String>(model);
		cbx.setBounds(10, 10, 250, 30);
		f.add(cbx);
		
		f.setVisible(true);
		
		// Hinzufügen eines Elementes:
		model.addElement("Kiwi");
		
		// Hinzufügen der Werte eines Sammelbehälters:
		String[] fruechte = {"Apfel", "Orange", "Erdbeere","Banane", "Pfirsich", "Pflaume"};
		for (int i = 0; i < fruechte.length; i++) {
			model.addElement(fruechte[i]);
		}
		
		// Element entfernen
		model.removeElementAt(3); // Erdbeere
		model.removeElement("Pfirsich");
		
		//*****************************************
		// Ansonsten: Siehe Experimente zur JList *
		//*****************************************
	}

	public static void main(String[] args) {
		new ComboBox1();
	}

}
