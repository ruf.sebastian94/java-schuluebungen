package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import meinzeug.MyFrame;

/*
	In dieser Klasse wird gezeigt, wie man eine komplexe
	JList erzeugt. 
	
	-	Die Klasse kann die Sammlung von Elementen aus der Datei �liste.txt� in der 
		ListBox ausgeben. (FileReader und BufferedReader)
	
	-	Die Elemente kann man anklicken und so in einem Ausgabe-Label Index 
		und Wert ausgeben.
	
	-	Man kann Elemente hinzuf�gen.
	
	-	Man kann die Elemente in einer Datei im Projektordner speichern (FileWriter),
		was die App auch �gleichzeitig� beendet. 
		
	-	Man kann das angeklickte Element per Knopfdruck l�schen, 
		was das Element aus der ListBox entfernt und aus dem Ausgabe-Label.
*/

public class ListBox4 implements ListSelectionListener, ActionListener {

	MyFrame f = new MyFrame(720, 500, "ListBox - JList");

	DefaultListModel<String> model = new DefaultListModel<String>();
	JList<String> lst;

	JLabel lbl;
	JTextField txtAnsEndeHinzufuegen;
	JButton btnAnsEndeHinzufuegen, btnAlleElementeLoeschen, btnListeInDateiSpeichern, btnRemove;

	public ListBox4() {// Konstruktor

		createControls();
		f.setVisible(true);

		listeAusDateiHolenUndInListBoxSchreiben();
	}

	private void createControls() {

		lst = new JList<String>(model);
		lst.setBounds(10, 10, 250, 320);
		lst.addListSelectionListener(this);
		f.add(lst);

		txtAnsEndeHinzufuegen = new JTextField();
		txtAnsEndeHinzufuegen.setBounds(270, 10, 200, 30);
		f.add(txtAnsEndeHinzufuegen);

		btnAnsEndeHinzufuegen = new JButton("an das Ende hinzuf�gen");
		btnAnsEndeHinzufuegen.setBounds(480, 10, 200, 30);
		btnAnsEndeHinzufuegen.addActionListener(this);
		f.add(btnAnsEndeHinzufuegen);

		btnAlleElementeLoeschen = new JButton("Alle Elemente l�schen");
		btnAlleElementeLoeschen.setBounds(270, 50, 410, 30);
		btnAlleElementeLoeschen.addActionListener(this);
		f.add(btnAlleElementeLoeschen);

		btnListeInDateiSpeichern = new JButton("in Datei speichern");
		btnListeInDateiSpeichern.setBounds(270, 300, 410, 30);
		btnListeInDateiSpeichern.addActionListener(this);
		f.add(btnListeInDateiSpeichern);

		btnRemove = new JButton("<==  dieses Element entfernen");
		btnRemove.setBounds(270, 370, 220, 30);
		btnRemove.addActionListener(this);
		f.add(btnRemove);

		lbl = new JLabel("Index: -1");
		lbl.setBounds(10, 370, 250, 30);
		f.add(lbl);

	}

	private void listeAusDateiHolenUndInListBoxSchreiben() {

		try {

			FileReader fr = new FileReader("liste.txt");
			BufferedReader br = new BufferedReader(fr);

			String zeile = br.readLine();

			while (zeile != null) {
				model.addElement(zeile);
				zeile = br.readLine();
			}

			br.close();
			fr.close();

		} catch (Exception e) {

			System.out.println("Beim Lesen der Datei lief etwas schief");
		}

	}

	private void listeInDateiSpeichern() {

		try {
			FileWriter fw = new FileWriter("liste.txt");

			// jede Zeile aus der ListBox in die Datei "liste.txt" schreiben:
			for (int i = 0; i < model.getSize(); i++) {
				fw.write(model.get(i) + "\n");
			}

			fw.close();

		} catch (IOException e) {
			System.out.println("Fehler beim Schreiben!");
		}

		JOptionPane.showMessageDialog(null, "Datei geschrieben");
		System.exit(0);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {// event handler des ListSelectionListeners

		int index = lst.getSelectedIndex();
		
		if (index > -1) {						
			String element = lst.getSelectedValue().toString();// St�rzt ab bei Index = -1
			lbl.setText("Index: " + index + "  |  Element: " + element);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {// event handler des ActionListeners

		if (e.getSource() == btnAnsEndeHinzufuegen) {
			model.addElement(txtAnsEndeHinzufuegen.getText());
			txtAnsEndeHinzufuegen.setText("");
		}

		if (e.getSource() == btnAlleElementeLoeschen) {
			model.clear();
		}

		if (e.getSource() == btnListeInDateiSpeichern) {
			listeInDateiSpeichern(); // Methodenaufruf
		}

		if (e.getSource() == btnRemove) {

			int index = lst.getSelectedIndex();
			if (index > -1) model.remove(index);

			lbl.setText("Index: -1"); 
		}

	}

	public static void main(String[] args) {
		new ListBox4();
	}
}
