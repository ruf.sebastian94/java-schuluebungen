package controls;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Spielstein extends JPanel implements MouseMotionListener {

	// Bereich f�r globale Variablen = Datenfelder = Felder = Attribute = ...
	int number;
	Spielfeld spielfeld;
	
	// Konstruktor
	public Spielstein(int xPosition, int yPosition, Color color, int nummer) {
		
		// Panel:
		this.setBounds(xPosition, yPosition, 40, 40);
		this.setBackground(color);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.setLayout(null);
		this.addMouseMotionListener(this);
	
		// Label:
		String[] buchstabe = {"A", "B", "C", "D", "E", "F", "G", "H", "I",
				              "J", "K", "L", "M", "N", "O", "P", "Q", "R", 
				              "S", "T", "U", "V", "W", "X", "Y", "Z"};
		
		JLabel lbl = new JLabel(buchstabe[nummer]);
		lbl.setBounds(1, 17, 30, 30);
		this.add(lbl);
		
		
		// Spielstein zeigen:
		this.setVisible(true);
	}
	
	
	public int getNumber() { // diese Methode wird derzeit nicht benutzt
		return this.number;
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		
		int mouseX = e.getX();
		int mouseY = e.getY();

		int steinX = this.getX();
		int steinY = this.getY();
		
		// mit der Integer-Division durch 20 und der sp�teren 
		// Multiplikation mit 20 erzeugen wir ein 20x20-Raster
		int neueSteinPositionX = (((mouseX + steinX) / 20) * 20);
		int neueSteinPositionY = (((mouseY + steinY) / 20) * 20);

		this.setLocation(neueSteinPositionX , neueSteinPositionY );
	}


	@Override
	public void mouseMoved(MouseEvent e) {	
	}

}
