package controls;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Spielfeld extends JFrame implements MouseListener {

	// Bereich f�r globale Variablen:
	Spielstein[] stein = new Spielstein[26];
	int sekunde = 0;
	Timer timer = new Timer();

	// Konstruktor:
	public Spielfeld() {
		
		createFrame();
		createSpielsteine();
		
		this.setVisible(true);

		// Timer: 10ms Verz�gerung & 1s periodisch
		timer.schedule(new Task(), 10, 1000);
	}

	public void createFrame() {
		this.setSize(1200, 800);
		// this.setTitle("Steinchenspiel");
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void createSpielsteine() {

		Random r = new Random();

		for (int i = 0; i < stein.length; i++) {

			int x = r.nextInt(1100);
			int y = r.nextInt(700);

			// f�r Pastellfarben: r, g, b = 156...255
			Color color = new Color(r.nextInt(156) + 100, r.nextInt(156) + 100, r.nextInt(156) + 100);

			// Steine erschaffen:
			stein[i] = new Spielstein(x, y, color, i); // i fungiert als Stein-Nummer
			stein[i].addMouseListener(this);
			this.add(stein[i]);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {

		// �ber welchem Spielstein liegt die Maus?
//		String[] buchstabe = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
//				"S", "T", "U", "V", "W", "X", "Y", "Z" };
//
//		for (int i = 0; i < buchstabe.length; i++) {
//			if (e.getSource() == stein[i]) {
//				this.setTitle("Stein Nr: " + i + " tr�gt den Buchstaben: " + buchstabe[i]);
//			}
//		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {

		
		boolean alleSteineSindAufDerselbenHoehe = true;
		boolean alleSteineSindInDerRichtigenReihenfolge = true;
		
		
		for (int i = 0; i < stein.length - 1; i++) {
			
			if (stein[i].getY() != stein[i + 1].getY()) {
				alleSteineSindAufDerselbenHoehe = false;
			}
			
			if (stein[i].getY() != stein[i + 1].getY()) {
				alleSteineSindInDerRichtigenReihenfolge = false;
			}
		}
		
		
		if(alleSteineSindAufDerselbenHoehe && alleSteineSindInDerRichtigenReihenfolge) {
			JOptionPane.showMessageDialog(null,"Hurrah - Sie ben�tigten "+ sekunde +" Sekunden");
			timer.cancel(); // Timer anhalten
		} 
		
		
		
		
//		if (stein[i].getX() > stein[i + 1].getX()) {
//			alleSteineSindaufDerselbenHoehe = false;
//		}
	}

	public static void main(String[] args) {
		new Spielfeld();
	}

	private class Task extends TimerTask {

		@Override
		public void run() {
			// Diese Methode wird sek�ndlich abgearbeitet.

			setTitle(sekunde + "s");

			sekunde++;

			// if(i>9) 

		} // end of run

	} // End of Task (eine innere Klasse)

} // end of class Spielfeld
