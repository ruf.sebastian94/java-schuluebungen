package model_view_controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import meinzeug.MyFrame;

/*
	Im Package "model_view_controller" realisieren wir das Prinzip
	"MVC: Model-View-Controller".
	
	Model: 		Datenbank
	View: 		dieses JFrame
	Controller: Datenbankzugriffsklasse
*/

public class View implements ActionListener, MouseListener {

	// ################################# Globale Variablen ###############################
	Datenbankzugriffsklasse db;
	MyFrame f = new MyFrame(850, 410, "Datenbankfrontend");
	JTextField txtDatenbankname, txtTabelleAnlegen;
	JButton btnDatenbankAnlegen, btnTabelleAnlegen, btnMitDatenbankVerbinden;
	JLabel[] lbl = new JLabel[4]; 			// Beschriftungslabels f�r insert, update & delete
	JTextField[] txt = new JTextField[4]; 	// TextFields f�r insert, update & delete
	JButton[] btn = new JButton[4]; 		// insert-, update- & delete-Button
	JTable table;
	DefaultTableModel model;
	JScrollPane scrollpane;

	// ################################# Konstruktor ######################################
	public View() {

		createControls();
		f.setVisible(true);
	}

	
	// ################################# eigene Methoden ##################################
	private void createControls() {

		// ########### Datenbank anlegen oder mit Datenbank verbinden: #############
		txtDatenbankname = new JTextField("datenbank.sqlite"); // Vorgeschlagener Name
		txtDatenbankname.setBounds(10, 10, 300, 30);
		f.add(txtDatenbankname);

		btnDatenbankAnlegen = new JButton("Datenbank anlegen");
		btnDatenbankAnlegen.setBounds(320, 10, 180, 30);
		btnDatenbankAnlegen.addActionListener(this);
		f.add(btnDatenbankAnlegen);

		btnMitDatenbankVerbinden = new JButton("mit Datenbank verbinden");
		btnMitDatenbankVerbinden.setBounds(510, 10, 180, 30);
		btnMitDatenbankVerbinden.addActionListener(this);
		f.add(btnMitDatenbankVerbinden);

		// ################## Tabelle anlegen: ##################
		String sql = "create table if not exists tblKunden" + "(id integer primary key, " + "vorname varchar(50), "
				+ "nachname varchar(50), " + "telefon varchar(25)) ";

		txtTabelleAnlegen = new JTextField(sql);
		txtTabelleAnlegen.setBounds(10, 50, 681, 30);
		f.add(txtTabelleAnlegen);
		btnTabelleAnlegen = new JButton("anlegen");
		btnTabelleAnlegen.setBounds(700, 50, 103, 30);
		btnTabelleAnlegen.addActionListener(this);
		f.add(btnTabelleAnlegen);

		// ################## Insert-, Update- und Delete-Controls ##################

		String[] beschriftungslabel = { "ID:", "Vorname:", "Nachname", "Telefon:" };
		String[] buttonbeschriftung = { "clear", "insert", "update", "delete" };

		int hor = 0; // horizontale Verschiebung

		for (int i = 0; i < 4; i++) {

			lbl[i] = new JLabel(beschriftungslabel[i]);
			lbl[i].setBounds(10 + hor, 100 + 30 * i, 100, 30);
			f.add(lbl[i]);

			txt[i] = new JTextField();
			txt[i].setBounds(100 + hor, 100 + 30 * i, 200, 28);
			f.add(txt[i]);

			btn[i] = new JButton(buttonbeschriftung[i]);
			btn[i].setBounds(10 + hor, 230 + 30 * i, 290, 24);
			btn[i].addActionListener(this);
			f.add(btn[i]);
		}

		lbl[0].setEnabled(false); // lblID
		txt[0].setEnabled(false); // txtID

	}

	private void createTable() {

		String[] columns = { "ID", "Vorname", "Nachname", "Telefon" };
		Object[][] data = new Object[0][4];

		model = new DefaultTableModel(data, columns);

		table = new JTable(model);
		table.setGridColor(Color.BLACK);
		table.addMouseListener(this);

		scrollpane = new JScrollPane(table);
		scrollpane.setBounds(320, 100, 485, 245);
		
		f.add(scrollpane);
	}
	
	private void fillTable(String sql) {
		
		// erstmal alle vorhandenen Zeilen wegputzen:
		model.setRowCount(0);
		
		
		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + txtDatenbankname.getText());
			conn.setAutoCommit(false);

			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery(sql); // unseren SQL-Befehl ausf�hren und Ergebnis in den Sammelbeh�lter rs f�llen

			Object[] data = new Object[4];
			
			while (rs.next()) {
				data[0] = rs.getObject("id"); // oder: rs.getObject(0);
				data[1] = rs.getObject("vorname");
				data[2] = rs.getObject("nachname");
				data[3] = rs.getObject("telefon");
				
				model.addRow(data); // Zeilen im JTable-Model hinzuf�gen
			}

			rs.close();
			statement.close();
			conn.close();
			
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	
	private void clearTextFields() {
		for (int i = 0; i < 4; i++)
			txt[i].setText("");
	}
		
	
	// ################################# event handler ####################################
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnDatenbankAnlegen) {
			db = new Datenbankzugriffsklasse(txtDatenbankname.getText());
			db.createDatabase();
		}

		if (e.getSource() == btnMitDatenbankVerbinden) {
			db = new Datenbankzugriffsklasse(txtDatenbankname.getText());
			db.createDatabase();

			createTable();
			fillTable("select * from tblKunden");
		}

		if (e.getSource() == btnTabelleAnlegen) {
			db.myExecuteUpdate(txtTabelleAnlegen.getText());
		}

		if (e.getSource() == btn[0]) { // btnClear
			clearTextFields();
		}

		if (e.getSource() == btn[1]) { // btnInsert 

			// Beispiel: "insert into tblKunden(vorname, nachname,
			// telefon) values('Max', 'Mustermann', '0351 1234567');"
			// txt[0] = txtID
			// txt[1] = txtVorname
			// txt[2] = txtNachname 
			// txt[3] = txtTelefon 
			
			String sql = "insert into tblKunden(vorname, nachname, telefon) values(" + 
						 "'" + txt[1].getText() + "', " + 
						 "'" + txt[2].getText() + "', " + 
						 "'" + txt[3].getText() + "');"; 
			
			db.myExecuteUpdate(sql);

			clearTextFields();
		}

		if (e.getSource() == btn[2]) { // btnUpdate
			clearTextFields();
		}

		if (e.getSource() == btn[3]) { // btnDelete
			clearTextFields();
		}
	}
		
	@Override
	public void mousePressed(MouseEvent e) {
		
		int zeile = table.getSelectedRow();
		
		for (int i = 0; i < 4; i++) {
			txt[i].setText(table.getValueAt(zeile, i).toString());
		}
				
	}
	
	
	// ################################# main ##############################################
	public static void main(String[] args) {
		new View();
	}
	
	
	// ################################# ungenutzte event handler ##########################

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
