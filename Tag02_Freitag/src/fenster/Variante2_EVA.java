package fenster;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Variante2_EVA implements ActionListener {

	// Bereich f�r globale Variablen:
	JFrame frm;
	JTextField txtEingabe;
	JButton btnVerarbeitung;
	JLabel lblAusgabe;
	
	
	// Konstruktor:
	public Variante2_EVA() {
		
		createFrame();
		createControls();
		
		frm.setVisible(true);		
	}
	
	
	private void createFrame() {
		frm = new JFrame();
		frm.setBounds(500, 300, 335, 180); // x, y, breite, h�he
		frm.setLayout(null);// Jetzt k�nnen Controls �ber x-y-Koordinaten platziert werden
		frm.setTitle("EVA");
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void createControls() {
		
		// TextBox, hier: JTextField
		txtEingabe = new JTextField();
		txtEingabe.setBounds(10, 10, 300, 30); // x, y, breite, h�he
		frm.add(txtEingabe);
		
		// Ein JButton, der h�bsch unter dem TextField platziert ist:
		btnVerarbeitung = new JButton();
		btnVerarbeitung.setBounds(10, 50, 300, 30);
		btnVerarbeitung.setText("Verarbeitung");
		btnVerarbeitung.addActionListener(this);
		frm.add(btnVerarbeitung);
		
		// Ein JLabel f�r die Ausgabe:
		lblAusgabe = new JLabel();
		lblAusgabe.setBounds(10, 90, 300, 30);
		lblAusgabe.setText(". . .");
		frm.add(lblAusgabe);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		lblAusgabe.setText( txtEingabe.getText() );
	    txtEingabe.setText(""); // TextField leer machen
	}

	public static void main(String[] args) {
		new Variante2_EVA(); // eigener Konstruktor
	}
}
