package fenster;

import javax.swing.*;

public class Variante1_MinimumFenster {
	
	// Konstruktor:
	public Variante1_MinimumFenster() {
		
		JFrame frm = new JFrame();
		frm.setBounds(500, 300, 335, 180); // x, y, breite, h�he
		frm.setTitle("Mein Fenster");
		frm.setLayout(null);               // Jetzt k�nnen Controls �ber x-y-Koordinaten platziert werden
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frm.setVisible(true);		
	}
	
	public static void main(String[] args) {
		new Variante1_MinimumFenster(); // eigener Konstruktor
	}

}
