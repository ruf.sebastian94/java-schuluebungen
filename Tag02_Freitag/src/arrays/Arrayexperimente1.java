package arrays;

import java.util.Arrays;

public class Arrayexperimente1 {

	public static void main(String[] args) {

		// eindimensionales Array (Vektor, Liste, Aufz�hlung)

		// Deklaration des Arrays f�r 3 ganze Zahlen:
		int[] a = new int[3]; // Indizes 0, 1 und 2 (keine 3!)

		// Wertezuweisungen:
		a[0] = 5;
		a[1] = 130;
		a[2] = 42;

		// Ausgabe mit For-Next-Schleife
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}

		System.out.println("\n-----------------------");

		// Ausgabe mit For-Each-Schleife
		for (int foo : a) {
			System.out.print(foo + " ");
		}

		// ***********************************************
		// Eigenschaften bei Arrays:
		System.out.println();
		System.out.println("L�nge des Arrays: " + a.length);

		Arrays.sort(a);

		System.out.println("\n-------\n sortiert:");

		// Ausgabe des sortierten Arrays mit For-Each-Schleife
		for (int foo : a) {
			System.out.print(foo + " ");
		}

	}

}
