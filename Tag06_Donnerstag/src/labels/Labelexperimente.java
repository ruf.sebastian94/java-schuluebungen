package labels;



import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

import meinzeug.MyFrame;

public class Labelexperimente {
	
	MyFrame f;
	
	
	public Labelexperimente() {
		
		f = new MyFrame(400, 400, "Labelexperimente");
		
		createControls(); // erschaffe Steuerelemente, hier: Labels
		
		f.setVisible(true);
		
	}

	private void createControls() {
		
		// Text als Konstruktorargument:
		JLabel lbl1 = new JLabel("Label1");
		lbl1.setBounds(10, 10, 100, 30);
		f.add(lbl1);
		
		// anstelle Kontruktorargument: Methode setText("...")
		JLabel lbl2 = new JLabel();
		lbl2.setBounds(10, 50, 100, 30);
		lbl2.setText("Label2");
		f.add(lbl2);
		
		// Schriftfarbe
		JLabel lbl3 = new JLabel();
		lbl3.setBounds(10, 90, 100, 30);
		lbl3.setText("Label3");
		lbl3.setForeground(Color.BLUE);
		f.add(lbl3);
		
		// Schriftart und Schriftgröße:
		JLabel lbl4 = new JLabel("Label4");
		lbl4.setBounds(10, 130, 300, 40);
		lbl4.setFont(new Font("Algerian", Font.PLAIN, 40));
		f.add(lbl4);
		
		// JLabel versteht HTML:
		
		JLabel lbl5 = new JLabel("<html>" 
								+"<body>"
								+ "<h1>Das JLabel</h1>"
								+ "<p>es kann auch<br>"
								+ "&nbsp; &nbsp; &nbsp; HTML!"
								+ "</p></body></html>");
		
		lbl5.setBounds(10, 130, 300, 300);
		f.add(lbl5);
		
		
		
	}

	public static void main(String[] args) {
		new Labelexperimente();
	}

}
