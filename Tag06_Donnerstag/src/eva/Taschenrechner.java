package eva;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import meinzeug.MyFrame;

public class Taschenrechner implements ActionListener {

	// Bereich f�r globale Variablen
	MyFrame f;
	JTextField txt1, txt2;
	JButton btnAddieren, btnSubtrahieren, btnMultiplizieren, btnDividieren;
	JLabel lblAusgabe;

	// Konstruktor
	public Taschenrechner() {

		f = new MyFrame(220, 400);
		createControls();
		f.setVisible(true);
	}

	private void createControls() {

		txt1 = new JTextField();
		txt2 = new JTextField();
		btnAddieren = new JButton("plus");
		btnSubtrahieren = new JButton("minus");
		btnMultiplizieren = new JButton("mal");
		btnDividieren = new JButton("durch");
		lblAusgabe = new JLabel(". . .");

		txt1.setBounds(50, 10, 100, 30);
		txt2.setBounds(50, 50, 100, 30);
		btnAddieren.setBounds(50, 90, 100, 30);
		btnSubtrahieren.setBounds(50, 130, 100, 30);
		btnMultiplizieren.setBounds(50, 170, 100, 30);
		btnDividieren.setBounds(50, 210, 100, 30);
		lblAusgabe.setBounds(50, 250, 100, 30);

		btnAddieren.addActionListener(this);
		btnSubtrahieren.addActionListener(this);
		btnMultiplizieren.addActionListener(this);
		btnDividieren.addActionListener(this);

		f.add(txt1);
		f.add(txt2);
		f.add(btnAddieren);
		f.add(btnSubtrahieren);
		f.add(btnMultiplizieren);
		f.add(btnDividieren);

		f.add(lblAusgabe);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		double a, b, c;

		a = Double.valueOf(txt1.getText());
		b = Double.valueOf(txt2.getText());

		c= -1;
		
		if (e.getSource() == btnAddieren) c = a + b;
		if (e.getSource() == btnSubtrahieren) c = a - b;
		if (e.getSource() == btnMultiplizieren) c = a * b;
		if (e.getSource() == btnDividieren) c = (double) a / (double) b;
		
		lblAusgabe.setText(String.valueOf(c));	
	}

	public static void main(String[] args) {
		new Taschenrechner();
	}

}
