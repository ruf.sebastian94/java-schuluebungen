package buttons;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import meinzeug.MyFrame;

public class Buttonexperimente1 implements ActionListener {
	
	MyFrame f;
	JButton btn;
	
	public Buttonexperimente1() {
		createFrame();
		createControls();
		
		f.setVisible(true);
	}

	private void createFrame() {
		f = new MyFrame();
		f.setTitle("Buttonexperimente");
	}

	private void createControls() {
		
		btn = new JButton();
		btn.setText("Taste");
		btn.setBounds(10, 10, 100, 30);
		btn.addActionListener(this);
		f.add(btn);
	}
	

	// Diese Methode ist der event handler des ActionListeners:
	@Override
	public void actionPerformed(ActionEvent e) {
		f.setTitle("Danke f�r's Klicken");
	}

	public static void main(String[] args) {
		new Buttonexperimente1();
	}
}
