package buttons;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import meinzeug.MyFrame;

public class Buttonexperimente2 implements ActionListener {

	MyFrame f;
	JButton btn1, btn2, btnEnde;

	public Buttonexperimente2() {
		createFrame();
		createControls();

		f.setVisible(true);
	}

	private void createFrame() {
		f = new MyFrame();
		f.setTitle("Buttonexperimente");
	}

	private void createControls() {

		btn1 = new JButton();
		btn1.setText("Taste 1");
		btn1.setBounds(10, 10, 100, 30);
		btn1.addActionListener(this);
		f.add(btn1);

		btn2 = new JButton();
		btn2.setText("Taste 2");
		btn2.setBounds(10, 50, 100, 30);
		btn2.setForeground(Color.BLUE); // Eigenschaften: �hnlich wie JLabel
		btn2.addActionListener(this);
		f.add(btn2);
		
		btnEnde = new JButton();
		btnEnde.setText("Ende");
		btnEnde.setBounds(10, 90, 100, 30);
		btnEnde.addActionListener(this);
		f.add(btnEnde);		
	}

	// Diese Methode ist der event handler des ActionListeners:
	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btn1) {
			f.setTitle("Danke f�r's Klicken");
		}
		
		if (e.getSource() == btn2) {
			f.setTitle("");
		}
		
		if (e.getSource() == btnEnde) {
			System.exit(0); // "0" meint: "Alles OK"
 		}
		
	}

	public static void main(String[] args) {
		new Buttonexperimente2();
	}
}
