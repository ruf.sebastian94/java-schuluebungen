package textfields;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import meinzeug.MyFrame;

public class TextFieldExperimente1 implements ActionListener {
	
	MyFrame f;
	JTextField txt;

	public TextFieldExperimente1() {
		createFrame();
		createControls();
		
		f.setVisible(true);
	}

	private void createFrame() {
		f = new MyFrame();
		f.setTitle("TextFieldExperimente");
	}

	private void createControls() {
		
		txt = new JTextField();
		//txt.setText(""); // Text im TextField st�rt oft
		txt.setBounds(10, 10, 350, 30);
		
		// Wenn man in das TextField Text eingibt, 
		// wird die Enter-Taste vom ActionListener detektiert:
		txt.addActionListener(this);  
		f.add(txt);
	}
		

	@Override
	public void actionPerformed(ActionEvent e) {
		f.setTitle(txt.getText());
		txt.setText(""); // TextField leer machen
	}
	
	public static void main(String[] args) {
		new TextFieldExperimente1();
	}

}
