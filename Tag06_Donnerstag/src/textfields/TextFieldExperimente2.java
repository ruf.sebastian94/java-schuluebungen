package textfields;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import meinzeug.MyFrame;

public class TextFieldExperimente2 implements ActionListener {

	MyFrame f;
	JTextField txt1, txt2;

	public TextFieldExperimente2() {
		createFrame();
		createControls();

		f.setVisible(true);
	}

	private void createFrame() {
		f = new MyFrame();
		f.setTitle("TextFieldExperimente");
	}

	private void createControls() {

		txt1 = new JTextField();
		txt1.setBounds(10, 10, 350, 30);
		txt1.addActionListener(this);
		f.add(txt1);

		txt2 = new JTextField();
		txt2.setBounds(10, 50, 350, 30);
		txt2.addActionListener(this);
		f.add(txt2);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == txt1) {
			f.setTitle(txt1.getText());
			txt1.setText(""); // TextField leer machen
		}
		
		if (e.getSource() == txt2) {
			f.setTitle(txt2.getText());
			txt2.setText(""); // TextField leer machen
		}
		
		
	}

	public static void main(String[] args) {
		new TextFieldExperimente2();
	}

}
