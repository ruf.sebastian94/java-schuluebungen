package datenbanken;
//Skript S. 185 ff.

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLite2 {

	String datenbankname = "db2.sqlite";

	public SQLite2() {

		// createDatabase();
		createTable();
		// insertDataset();
		// deleteDataset();
		// updateDataset();
		// einzelwertHolen(); // z.B. min(id), max(id), count(*) etc.
		// einenDatensatzHolen();

	}

	private void myExecuteUpdate(String sql) {
		// create Table, insert, update, delete etc.

		try {

			Class.forName("org.sqlite.JDBC");

			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + datenbankname);

			Statement statement = conn.createStatement();

			statement.executeUpdate(sql); // SQL-Befehl ausf�hren

			statement.close();

			conn.close();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private void einenDatensatzHolen() {

		System.out.println("einen Datensatz holen ...");

		try {

			Class.forName("org.sqlite.JDBC");

			// Die n�chste Zeile versucht, eine Verbindung zu einer Datenbank
			// herzustellen. Eine nichtvorhandene Datenbank w�rde angelegt werden.
			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + datenbankname);
			conn.setAutoCommit(false);

			String sql = "select * from tblKunden where id=1;";

			Object[] data = new Object[4];

			Statement statement = conn.createStatement();

			ResultSet rs = statement.executeQuery(sql); // unseren SQL-Befehl ausf�hren und Ergebnis in den Sammelbeh�lter rs f�llen

			while (rs.next()) {
				data[0] = rs.getObject("id"); // oder: rs.getObject(0);
				data[1] = rs.getObject("vorname");
				data[2] = rs.getObject("nachname");
				data[3] = rs.getObject("telefon");
			}

			rs.close();
			statement.close();
			conn.close();

			for (int i = 0; i < data.length; i++) {
				System.out.print(data[i] + "   ");
			}

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private void einzelwertHolen() {

		System.out.println("Einzelwert holen ...");

		try {

			Class.forName("org.sqlite.JDBC");

			// Die n�chste Zeile versucht, eine Verbindung zu einer Datenbank
			// herzustellen. Eine nichtvorhandene Datenbank w�rde angelegt werden.
			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + datenbankname);

			String sql = "select count(*) from tblKunden;";
			Object einzelwert;

			Statement statement = conn.createStatement();

			ResultSet rs = null;

			rs = statement.executeQuery(sql); // unseren SQL-Befehl ausf�hren

			rs.next();

			einzelwert = rs.getObject(1);

			rs.close();
			statement.close();
			conn.close();

			System.out.println("der geholte Wert: " + einzelwert.toString());

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	private void updateDataset() {

		myExecuteUpdate("update tblKunden set telefon='030 2222222' where id=1;");
	}

	private void deleteDataset() {

		myExecuteUpdate("delete from tblKunden where id=2;");
	}

	private void insertDataset() {

		String sql = "insert into tblKunden(vorname, nachname, telefon) values" +
				     "('Olga', 'Kommofssofa', '0351 4666887')";

		myExecuteUpdate(sql);
	}

	private void createTable() {

		String sql = "create table if not exists tblKunden" + "(id integer primary key, " + "vorname varchar(50), "
				+ "nachname varchar(50), " + "telefon varchar(25)) ";

		myExecuteUpdate(sql);
	}

	private void createDatabase() {

		System.out.println("Die Datenbank wird angelegt...");

		try {

			Class.forName("org.sqlite.JDBC");

			// Die n�chste Zeile versucht, eine Verbindung zu einer Datenbank
			// herzustellen. Eine nichtvorhandene Datenbank w�rde angelegt werden.
			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + datenbankname);

			conn.close();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new SQLite2();
	}
}
