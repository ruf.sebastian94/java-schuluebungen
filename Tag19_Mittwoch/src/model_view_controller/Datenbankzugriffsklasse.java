package model_view_controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Datenbankzugriffsklasse {

	// Bereich f�r globale Variablen = Attribut = Feld = Klassenvariable
	String datenbankname;
	
	
	// Konstruktor
	public Datenbankzugriffsklasse(String datenbankname) {
		
		// glob. Variable <== Parameter 
		this.datenbankname = datenbankname;
	}
	
	public void createDatabase() {

		//System.out.println("Die Datenbank wird angelegt...");

		try {

			Class.forName("org.sqlite.JDBC");

			// Die n�chste Zeile versucht, eine Verbindung zu einer Datenbank
			// herzustellen. Eine nichtvorhandene Datenbank w�rde angelegt werden.
			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + datenbankname);

			conn.close();
			
			// System.out.println("fertsch...");

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public void myExecuteUpdate(String sql) {
		// myExecuteUpdate kann: create Table, insert, update, delete etc.

		try {

			Class.forName("org.sqlite.JDBC");

			Connection conn = DriverManager.getConnection("jdbc:sqlite:" + datenbankname);

			Statement statement = conn.createStatement();

			statement.executeUpdate(sql); // SQL-Befehl ausf�hren

			statement.close();

			conn.close();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}


}
