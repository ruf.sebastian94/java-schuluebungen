package controls;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import meinzeug.MyFrame; // aus dem Projekt "Tools"

public class Karteikarten implements ActionListener {

	// Bereich f�r globale Variablen
	MyFrame f = new MyFrame(800, 300, "Karteikarten - JTabbedPane");
	JTabbedPane tp = new JTabbedPane();
	JPanel panel1, panel2, panel3;
	JTextField txt;
	JButton btn;
	JLabel lbl;

	// Konstruktor
	public Karteikarten() {

		createKarteikarten();

		f.setVisible(true);
	}

	private void createKarteikarten() {

		// Wir w�hlen f�r das Frame ein Layout, dass
		// die Steuerelemente (JPanels) maximiert:
		f.setLayout(new BorderLayout());

		// Instanziierung der drei Panels:
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();

		// panels erhalten null-Layout,
		// f�r die x-y-Platzierung auf ihnen:
		panel1.setLayout(null);
		panel2.setLayout(null);
		panel3.setLayout(null);

		txt = new JTextField("Dieses TextField befindet sich auf Karteikarte 1");
		btn = new JButton("Dieser Button befindet sich auf Karteikarte 2");
		lbl = new JLabel("Dieses Label befindet sich auf Karteikarte 3");

		txt.setBounds(10, 100, 750, 30);
		btn.setBounds(10, 100, 750, 30);
		lbl.setBounds(10, 100, 750, 30);

		btn.addActionListener(this);

		panel1.add(txt);
		panel2.add(btn);
		panel3.add(lbl);

		tp.addTab("Karteikarte 1", panel1);
		tp.addTab("Karteikarte 2", panel2);
		tp.addTab("Karteikarte 3", panel3);

		f.add(tp);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		lbl.setText(txt.getText());

	}

	public static void main(String[] args) {
		new Karteikarten();
	}
}
