package controls;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import meinzeug.MyFrame; // aus dem Projekt "Tools"

public class KarteikartenUebung {

	// Bereich f�r globale Variablen
	MyFrame f = new MyFrame(1700, 300, "Karteikarten - JTabbedPane");
	JTabbedPane tp = new JTabbedPane();
	JPanel[] pan = new JPanel[31];
	
	// Konstruktor
	public KarteikartenUebung() {

		createKarteikarten();

		f.setVisible(true);
	}

	private void createKarteikarten() {

		// Wir w�hlen f�r das Frame ein Layout, dass
		// die Steuerelemente (JPanels) maximiert:
		f.setLayout(new BorderLayout());

		// Instanziierung der drei Panel[1...30]:
		for (int i = 1; i <= 30; i++) {
			pan[i] = new JPanel();
			tp.addTab("pan" + i, pan[i]);
		}

		f.add(tp);
	}
	
	public static void main(String[] args) {
		new KarteikartenUebung();
	}
}
