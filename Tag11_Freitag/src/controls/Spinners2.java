package controls;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import meinzeug.MyFrame;

/*
	In dieser Klasse soll gezeigt werden,

	- wie man mittels SpinnerNumberModel Startwert, 
	  Min, Max, Schrittweite festlegt
	
	- wie man mittels SpinnerListModel Listen, wie z. B. Wochentage 
	  benutzen kann

*/

public class Spinners2 implements ChangeListener {

	MyFrame f = new MyFrame(300, 200, "JSpinner");
	JSpinner spn1, spn2;
	JLabel lbl1, lbl2;
	SpinnerNumberModel numberModel; // f�r Minimum, Maximum etc.
	SpinnerListModel listModel;

	public Spinners2() {

		// Startwert, Minimum, Maximum, Schrittweite
		numberModel = new SpinnerNumberModel(0, -10, 10, 2);

		spn1 = new JSpinner(numberModel);
		spn1.setBounds(10, 10, 100, 30);
		spn1.addChangeListener(this);
		f.add(spn1);

		lbl1 = new JLabel("0");
		lbl1.setBounds(120, 10, 100, 30);
		f.add(lbl1);

		String[] tag = { "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag" };

		listModel = new SpinnerListModel(tag);

		spn2 = new JSpinner(listModel);
		spn2.setBounds(10, 50, 100, 30);
		spn2.addChangeListener(this);
		f.add(spn2);

		lbl2 = new JLabel("Montag");
		lbl2.setBounds(120, 50, 100, 30);
		f.add(lbl2);

		f.setVisible(true);
	}

	@Override
	public void stateChanged(ChangeEvent e) { // event handler vom ChageListener
		lbl1.setText(spn1.getValue().toString());
		lbl2.setText(spn2.getValue().toString());
	}

	public static void main(String[] args) {
		new Spinners2();
	}
}
