package controls;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import meinzeug.MyFrame;

public class Spinners1 implements ChangeListener {

	MyFrame f = new MyFrame(300, 200, "JSpinner");
	JSpinner spn;
	JLabel lbl;
		
	public Spinners1() {
		
		spn = new JSpinner();
		spn.setBounds(10, 10, 100, 30);
		spn.addChangeListener(this);
		f.add(spn);
		
		lbl = new JLabel("0");
		lbl.setBounds(120, 10, 100, 30);
		f.add(lbl);	
		
		f.setVisible(true);
	}
	
	@Override
	public void stateChanged(ChangeEvent e) { // event handler vom ChageListener
		lbl.setText(spn.getValue().toString());
		
	}

	public static void main(String[] args) {
		new Spinners1();
	}
}
