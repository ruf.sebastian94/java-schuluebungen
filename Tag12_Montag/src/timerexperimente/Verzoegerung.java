package timerexperimente;

import java.util.Timer;
import java.util.TimerTask;

public class Verzoegerung {

	// Bereich f�r globale Variablen:
	Timer t = new Timer();
	
	
	// Konstruktor:
	public Verzoegerung() {
		
		System.out.println("Start ist erfolgt");
		
		t.schedule(new Task(), 5000); // 5000ms = 5s
		
		System.out.println("Programm befindet sich nach Timerstart");
		
	}

	public static void main(String[] args) {
		new Verzoegerung(); // den eigenen Kostruktor aufrufen

	}
}

class Task extends TimerTask {

	@Override
	public void run() {
		// Diese Methode wird immer dann abgearbeitet, 
		// wenn der Timer es befiehlt.
		
		System.out.println("5s sind um.");	
	}
	
	
}