package timerexperimente;

import java.util.Timer;
import java.util.TimerTask;

public class PeriodischesAbarbeiten {

	// Bereich f�r globale Variablen:
	Timer t = new Timer();
	int i = 0; // Anzahl der Abarbeitungen

	// Konstruktor:
	public PeriodischesAbarbeiten() {

		System.out.println("Start ist erfolgt");

		t.schedule(new Task(), 5000, 1000); // 5s Verz�gerung & 1s Periode

		System.out.println("Programm befindet sich nach Timerstart");

	}

	public static void main(String[] args) {
		new PeriodischesAbarbeiten(); // den eigenen Kostruktor aufrufen

	}

	private class Task extends TimerTask {

		@Override
		public void run() {
			// Diese Methode wird immer dann abgearbeitet,
			// wenn der Timer es befiehlt.

			// sek�ndlich aber nach 5s Erst-Verz�gerung:
			System.out.println(i + ". immer wieder");

			// bei i == 10 ist Schluss
			i++;
			if(i>9) t.cancel();
			
		} // end of run 
	}   // End of Task2 (ist jetzt eine innere Klasse)  
 }// End of PeriodischesAbarbeiten
