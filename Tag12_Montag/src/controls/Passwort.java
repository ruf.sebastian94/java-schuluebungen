package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import meinzeug.MyFrame;

public class Passwort implements ActionListener {

	// Bereich f�r globale Variablen
	MyFrame f = new MyFrame(430, 250, "JPasswortField");
	JPasswordField pwf;
	JButton btn;
		
	
	// Konstruktor:
	public Passwort() {
	
		createControls();
		
		f.setVisible(true);
	}

	private void createControls() {
		
		pwf = new JPasswordField();
		pwf.setBounds(10, 10, 300, 30);
		f.add(pwf);
		
		btn = new JButton("OK");
		btn.setBounds(320, 10, 80, 30);
		btn.addActionListener(this);
		f.add(btn);
	}

	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		char[] correctPassword = {'J', 'a', 'v', 'a'};
		char[] inputPassword = pwf.getPassword();
		
		boolean correct = true;
		
		if (inputPassword.length == correctPassword.length) {
			
			for (int i = 0; i < inputPassword.length; i++) {
				
				if(inputPassword[i] != correctPassword[i]) correct = false;
			}
			
		} else {
			
			correct = false; // unterschiedliche L�nge
		}
		
		
		if (correct) {
			JOptionPane.showMessageDialog(null, "richtig");
			// Hier k�nnte es dann weitergehen
		} else {
			JOptionPane.showMessageDialog(null, "falsches Passwort");
		}
		
		
		
	}

	
	public static void main(String[] args) {
		new Passwort();
	}
}
