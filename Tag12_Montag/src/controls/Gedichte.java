package controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


import meinzeug.*; // MyFrame & TextAusgabeMassentext

public class Gedichte implements ActionListener {

	MyFrame f = new MyFrame(340, 170, "Gedichte");
	JButton[] btn = new JButton[3];
	String[] gedicht = new String[3];
	TextAusgabeMassentext[] ausgabe = new TextAusgabeMassentext[3];

	// Konstruktor:
	public Gedichte() {

		String[] beschriftung = { "Auerhahn", "Ottos Mops", "Das gro�e Lalula" };
		
		erzeugeGedichteStrings();
		
		for (int i = 0; i < 3; i++) {
		
			// Buttons auf dem Master:
			btn[i] = new JButton(beschriftung[i]);
			btn[i].setBounds(10, 15 + 35 * i, 300, 30);
			btn[i].addActionListener(this);
			f.add(btn[i]);
			
			// Ausgabeframes:
			 ausgabe[i] = new TextAusgabeMassentext(gedicht[i]);
		}
			
				
		f.setVisible(true);
	}

	private void erzeugeGedichteStrings() {
		
		gedicht[0] =  "Otto Waalkes - Der Auerhahn \n \n"
					+ "Im Walde sitzt ein Auerhahn,\n"
					+ " der schaut mich ganz sch�n sauer an.\n"
					+ "Das st�rt mich nicht weil ich jetzt penne\n"
					+ "und zwar auf seiner Auerhenne.\n";

		gedicht[1] = "Ernst Jandl - Ottos Mops\n \n" +
					 "ottos mops trotzt\n" + 
					 "otto: fort mops fort\n" + 
					 "ottos mops hopst fort\n" + 
					 "otto: soso\n" + 
					 "otto holt koks\n" + 
					 "otto holt obst\n" + 
					 "otto horcht\n" + 
					 "otto: mops mops\n" + 
					 "otto hofft\n" + 
					 "ottos mops klopft\n" + 
					 "otto: komm mops komm\n" + 
					 "ottos mops kommt\n" + 
					 "ottos mops kotzt\n" + 
					 "otto: ogottogott\n";
		
		gedicht[2] = "Christian Morgenstern - Das gro�e Lalula\n\n" + 
					 "Kroklokwafzi? Semmememmi!\n" + 
					 "Seiokrontro - prafriplo:\n" + 
					 "Bifzi, bafzi; hulalemmi:\n" + 
					 "quasti basti bo...\n" + 
					 "Lalu lalu lalu lalu la!\n" + 
					 "Hontraruru miromente\n" + 
					 "zasku zes r� r�?\n" + 
					 "Entepente, leiolente\n" + 
					 "klekwapufzi l�?\n" + 
					 "Lalu lalu lalu lala la!\n" + 
					 "Simarar kos malzipempu\n" + 
					 "silzuzankunkrei (;)!\n" + 
					 "Marjomar dos: Quempu Lempu\n" + 
					 "Siri Suri Sei []!\n" + 
					 "Lalu lalu lalu lalu la!\n";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		for (int i = 0; i < 3; i++) {	
			if(e.getSource() == btn[i]) {
				 ausgabe[i].setVisible(true);
			}
		}
	}

	public static void main(String[] args) {
		new Gedichte();
	}
}
