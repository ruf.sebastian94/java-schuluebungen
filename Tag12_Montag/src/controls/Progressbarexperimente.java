package controls;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JProgressBar;

import meinzeug.MyFrame;


public class Progressbarexperimente {

	// Bereich f�r globale Variablen:
	MyFrame f = new MyFrame(500, 300, "JProgressbar");
	JProgressBar bar = new JProgressBar();
	Timer t = new Timer();
	int i=0;
	
	
	
	// Konstruktor:
	public Progressbarexperimente() {

		t.schedule(new Task(), 0, 50); // 0s Verz�gerung & 1s Periode
		
		bar.setBounds(10, 10, 460, 30);
		bar.setValue(42);
		f.add(bar);
		

		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Progressbarexperimente();
	}

	
	private class Task extends TimerTask {

		@Override
		public void run() {
			// Diese Methode wird immer dann abgearbeitet,
			// wenn der Timer es befiehlt.
			
			i++;			
			bar.setValue(i);
			
			if(i>100) t.cancel(); // bei 100 ist Schluss
			
		} // end of run 
	}   // End of Task (ist jetzt eine innere Klasse)  
	
}
