package controls;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import meinzeug.MyFrame;

public class TextAreaExperimente {

	MyFrame f = new MyFrame(500, 300, "JTextArea");
	
	public TextAreaExperimente() {
		
		f.setLayout(new BorderLayout()); // nutzt die gesamte JFame-Fl�che
		
		createControls();
		
		f.setVisible(true);
	}

	private void createControls() {
		
		JTextArea txt = new JTextArea();
		
		txt.setLineWrap(true); // Umbrechen erlauben
		txt.setWrapStyleWord(true);
		
		txt.setText("Hallo, ich bin in einer JTextArea \n \n"
				+ "Video bietet eine leistungsstarke M�glichkeit zur Unterst�tzung "
				+ "Ihres Standpunkts. Wenn Sie auf \"Onlinevideo\" klicken, k�nnen "
				+ "Sie den Einbettungscode f�r das Video einf�gen, das hinzugef�gt "
				+ "werden soll. Sie k�nnen auch ein Stichwort eingeben, um online "
				+ "nach dem Videoclip zu suchen, der optimal zu Ihrem Dokument "
				+ "passt. Damit Ihr Dokument ein professionelles Aussehen erh�lt, "
				+ "stellt Word einander erg�nzende Designs f�r Kopfzeile, Fu�zeile, "
				+ "Deckblatt und Textfelder zur Verf�gung. Beispielsweise k�nnen "
				+ "Sie ein passendes Deckblatt mit Kopfzeile und Randleiste "
				+ "hinzuf�gen. Klicken Sie auf \"Einf�gen\", und w�hlen Sie "
				+ "dann die gew�nschten Elemente aus den verschiedenen Katalogen "
				+ "aus. Designs und Formatvorlagen helfen auch dabei, die Elemente "
				+ "Ihres Dokuments aufeinander abzustimmen. Wenn Sie auf \"Design\" "
				+ "klicken und ein neues Design ausw�hlen, �ndern sich die Grafiken,"
				+ " Diagramme und SmartArt-Grafiken so, dass sie dem neuen Design "
				+ "entsprechen. Wenn Sie Formatvorlagen anwenden, �ndern sich die "
				+ "�berschriften passend zum neuen Design. Sparen Sie Zeit in Word "
				+ "dank neuer Schaltfl�chen, die angezeigt werden, wo Sie sie "
				+ "ben�tigen. Zum �ndern der Weise, in der sich ein Bild in Ihr "
				+ "Dokument ");
		
		f.add(new JScrollPane(txt) );
		
	}

	public static void main(String[] args) {
		new TextAreaExperimente();
	}
}
