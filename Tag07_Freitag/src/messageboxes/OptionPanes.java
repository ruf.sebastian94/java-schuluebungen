package messageboxes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import meinzeug.MyFrame;

/*
	In dieser Klasse sollen vier Dialoge gezeigt werden:
	- Message Dialog (�hnlich C# MessageBox)
	- Confirmation Dialog (Best�tigung �ber Buttons)
	- Input Dialog (mit TextField)
	- Option Dialog (eigene Buttons)
*/

public class OptionPanes implements ActionListener {

	MyFrame f;
	JButton btn1, btn2, btn3, btn4;

	// Konstruktor
	public OptionPanes() {

		f = new MyFrame(287, 215, "JOptionPanes");

		createButtons();

		f.setVisible(true);

	}

	private void createButtons() {

		btn1 = new JButton("Message Dialog");
		btn1.setBounds(10, 10, 250, 30);
		btn1.addActionListener(this);
		f.add(btn1);

		btn2 = new JButton("Confirmation Dialog");
		btn2.setBounds(10, 50, 250, 30);
		btn2.addActionListener(this);
		f.add(btn2);

		btn3 = new JButton("Input Dialog");
		btn3.setBounds(10, 90, 250, 30);
		btn3.addActionListener(this);
		f.add(btn3);

		btn4 = new JButton("Option Dialog");
		btn4.setBounds(10, 130, 250, 30);
		btn4.addActionListener(this);
		f.add(btn4);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == btn1) {
			JOptionPane.showMessageDialog(null, "Das ist die Nachricht");
		}

		if (e.getSource() == btn2) {
			int eingabe = JOptionPane.showConfirmDialog(null, 
					                      "Best�tigen Sie dies?", 
					                      "Frage",
					                      JOptionPane.YES_NO_CANCEL_OPTION); 
			
			
			
			if(eingabe == -1) btn2.setText("rotes X");
			if(eingabe == 0) btn2.setText("Ja");
			if(eingabe == 1) btn2.setText("Nein");
			if(eingabe == 2) btn2.setText("Abbrechen");	
		}

		if (e.getSource() == btn3) {
			String eingabe = JOptionPane.showInputDialog("Geben Sie Ihren Vornamen ein");
			btn3.setText(eingabe);
		}

		if (e.getSource() == btn4) {
			
			// Ein Array f�r die Buttons (Optionen)
			String[] options = {"Ja", "Nein", "vielleicht", "Muss ich erst meine Frau fragen"};
			
			
			int ausgewaehlt = JOptionPane.showOptionDialog(null, 
					                     "Haben Sie in Ihrer Partnerschaft die Hosen an?", 
					                      "Die entscheidende Frage",
					                      JOptionPane.DEFAULT_OPTION,
					                      JOptionPane.QUESTION_MESSAGE,
					                      null, options, options[3]);
			
			btn4.setText("" + ausgewaehlt);
		}

	}

	public static void main(String[] args) {
		new OptionPanes();
	}

}
