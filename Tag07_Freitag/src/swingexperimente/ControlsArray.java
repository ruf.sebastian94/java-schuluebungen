package swingexperimente;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

// In dieser Klasse soll gezeigt werden, wie man 
// aus Steuerelementen ein Array macht.

import meinzeug.MyFrame; // aus BuildPath: Tools.meinzeug.MyFrame

public class ControlsArray implements ActionListener {

	MyFrame f;
	JTextField[] txt = new JTextField[25]; 
	JButton[] btn = new JButton[25]; 	
	JLabel[] lbl = new JLabel[25]; 

	// Konstruktor
	public ControlsArray() {

		f = new MyFrame(400, 900, "ControlsArray");

		createControls();

		f.setVisible(true);
	}

	private void createControls() {

		for (int i = 0; i < lbl.length; i++) {
	
			txt[i] = new JTextField();
			txt[i].setBounds(10, 10 + 30*i, 100, 30);
			f.add(txt[i]);
			
			btn[i] = new JButton("Button " + i);
			btn[i].setBounds(120, 10 + 30*i, 100, 30);
			btn[i].addActionListener(this);
			f.add(btn[i]);			
			
			lbl[i] = new JLabel("Label " + i);
			lbl[i].setBounds(240, 10 + 30*i, 100, 30);
			f.add(lbl[i]);
		}
	}

	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		for (int i = 0; i < lbl.length; i++) {
			if(e.getSource() == btn[i]) lbl[i].setText(txt[i].getText());
		}
		
	}
	
	public static void main(String[] args) {
		new ControlsArray();
	}

}
