package meinzeug;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class TextAusgabeMassentext extends JFrame {

	// Konstruktor 
	public TextAusgabeMassentext(String ausgabetext) {
		
		this.setSize(250, 300);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
						
		JTextArea txtarea = new JTextArea();
		txtarea.setLineWrap(true); 			// Umbrechen erlauben
		txtarea.setWrapStyleWord(true); 	// Worte nicht zerhacken
		txtarea.setText(ausgabetext);
		this.add(new JScrollPane(txtarea));
	}
}
