package meinzeug;

import java.awt.Color;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	// 1. �berladung
	public MyFrame() {
		this.setSize(400, 400); // breite, h�he
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// 2. �berladung
	public MyFrame(int breite, int hoehe) {
		this.setSize(breite, hoehe);
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// 3. �berladung
	public MyFrame(int breite, int hoehe, String titel) {
		this.setSize(breite, hoehe);
		this.setTitle(titel);
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// 4. �berladung
	public MyFrame(int breite, int hoehe, String titel, Color hintergrundfarbe) {
		this.setSize(breite, hoehe);
		this.setTitle(titel);
		this.getContentPane().setBackground(hintergrundfarbe);
		this.setLocationRelativeTo(null);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// 5. �berladung
	public MyFrame(int xPos, int yPos, int breite, int hoehe, Color hintergrundfarbe) {
		this.setBounds(xPos, yPos, breite, hoehe);
		this.getContentPane().setBackground(hintergrundfarbe);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}

	

}
